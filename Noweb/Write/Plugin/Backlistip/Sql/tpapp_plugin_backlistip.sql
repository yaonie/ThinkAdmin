CREATE TABLE IF NOT EXISTS `tka_plugin_backlistip` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `ip` char(15) NOT NULL COMMENT 'IP地址',
  `locktime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '锁定截止时间 0为永久锁定',
  `lockmodule` varchar(200) NOT NULL COMMENT '锁定模块',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_UNIQUE` (`ip`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='IP黑名单表' AUTO_INCREMENT=1 ;