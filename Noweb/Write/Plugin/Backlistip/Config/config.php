<?php 
return array (
  'NAME' => 'IP黑名单插件',
  'TYPE' => 'plugin',
  'STATUS' => 2,
  'REMARK' => 'IP黑名单插件可以限制指定IP访问,并且详细控制限制到某一秒钟等。限制了的IP在规定时间内不能访问站点。可以很大程度提高网站安全',
  'MENUSQL' => 
  array (
    0 => 
    array (
      'name' => 'IP黑名单',
      'pid' => 6,
      'sort' => 1,
      'status' => 0,
      'menutype_id' => 1,
      'rules' => 'Admin-Plugin-Backlistip',
      'rulestype' => 'and',
    ),
    1 => 
    array (
      'name' => '管理IP黑名单',
      'pid' => 'key0',
      'url' => 'Admin/Extension/usehook?type=plugin&name=Backlistip&hookc=Run&hooka=index',
      'fun' => 1,
      'sort' => 1,
      'status' => 0,
      'menutype_id' => 1,
      'rules' => 'Admin/Extension/usehook/Plugin/Backlistip/Run/index',
      'rulestype' => 'and',
    ),
  ),
  'MENUID' => 
  array (
    0 => 27,
    1 => 28,
  ),
  'RULESSQL' => 
  array (
    0 => 
    array (
      'title' => 'IP黑名单插件',
      'rules' => 'Admin-Plugin-Backlistip',
      'pid' => 9,
      'status' => 0,
      'sort' => 1,
      'remark' => 'IP黑名单插件菜单权限规则',
    ),
    1 => 
    array (
      'title' => '配置IP黑名单',
      'rules' => 'Admin/Extension/usehook/Plugin/Backlistip/Run/config',
      'pid' => 'key0',
      'status' => 0,
      'sort' => 1,
      'remark' => '配置IP黑名单插件权限规则',
    ),
    2 => 
    array (
      'title' => '管理IP黑名单',
      'rules' => 'Admin/Extension/usehook/Plugin/Backlistip/Run/index',
      'pid' => 'key0',
      'status' => 0,
      'sort' => 1,
      'remark' => '管理IP黑名单插件权限规则',
    ),
    3 => 
    array (
      'title' => '添加IP黑名单',
      'rules' => 'Admin/Extension/usehook/Plugin/Backlistip/Run/add',
      'pid' => 'key2',
      'status' => 0,
      'sort' => 1,
      'remark' => '添加IP黑名单插件权限规则',
    ),
    4 => 
    array (
      'title' => '编辑IP黑名单',
      'rules' => 'Admin/Extension/usehook/Plugin/Backlistip/Run/edit',
      'pid' => 'key2',
      'status' => 0,
      'sort' => 1,
      'remark' => '编辑IP黑名单插件权限规则',
    ),
    5 => 
    array (
      'title' => '删除IP黑名单',
      'rules' => 'Admin/Extension/usehook/Plugin/Backlistip/Run/delete',
      'pid' => 'key2',
      'status' => 0,
      'sort' => 1,
      'remark' => '删除IP黑名单插件权限规则',
    ),
  ),
  'RULESID' => 
  array (
    0 => 53,
    1 => 54,
    2 => 55,
    3 => 56,
    4 => 57,
    5 => 58,
  ),
  'TABEL' => 
  array (
    0 => 'plugin_backlistip',
  ),
  'SQLLIST' => 
  array (
    0 => 'tpapp_plugin_backlistip.sql',
  ),
) 
?>