<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |IP黑名单插件入口地址
 * +--------------------------------------------------------------------------------------------
 * |P黑名单插件可以限制指定IP访问,并且详细控制限制到某一秒钟等。限制了的IP在规定时间内不能访问站点
 * |可以很大程度提高网站安全
 * +--------------------------------------------------------------------------------------------
 */
namespace Write\Plugin\Backlistip\Controller;
use Common\Controller\CommonController;
class Run extends CommonController{

	public $access = array('verifybacklistip'); //不允许外部访问的操作

	/**
	 * 验证IP黑名单是否通过
	 */
	function verifybacklistip(){
		$mBacklistip = new \Write\Plugin\Backlistip\Model\PluginBacklistipModel;
		//获取验证数据
		$lock	=	$mBacklistip ->getLock(get_client_ip());
		if($lock['time'] >= 0){
			if(in_array(MODULE_NAME,  $lock['module'])){ //在锁定的模块内
				if($lock['time'] ==0 ) $this->msg(0,L('THISIP_PERMANENT_BAN_TIP'),'','false');  //IP已被永久禁止
				elseif($lock['time'] > time())
				$this->msg(0,L('THISIP__BAN_TIME_TIP').date('Y-m-d H:i:s', $lock['time']),'','false');  //IP已被时间限制禁止
			}
		}
	}

	/**
	 *
	 * 配置IP黑名单插件
	 * @remark 配置IP黑名单插件
	 */
	public function config(){
		$this->msg(0, L('THIS_PLUGIN_NO_CONFIG'));
	}
	
	/**
	 * 管理IP黑名单
	 * @remark 管理IP黑名单
	 */
	public function index(){
		$mBacklistip = new \Write\Plugin\Backlistip\Model\PluginBacklistipModel;
		$limit = I('get.limit');
		!$limit &&  $limit = C('LIMIT');
		$where  = $this->get_where();  // 条件初始化
		$list = $mBacklistip->where($where)->order(I('get.ordertype','id').' '.I('get.orderval','desc'))->page(I('get.'.C('VAR_PAGE'),0).','.$limit)->select();
		$this->assign('list',$list);// 赋值数据集
		$count      = $mBacklistip->where($where)->count();// 查询满足要求的总记录数
		$pageClass =  '\Common\Lib\Page'.str_replace('-', '', LANG_SET);
		$Page       = new $pageClass($count, $limit);// 实例化分页类 传入总记录数和每页显示的记录数
		$show       = $Page->show();// 分页显示输出
		$this->assign('page', $show);
		$this->display($this->path.'View/'.MODULE_NAME.'/'.__FUNCTION__.C('TMPL_TEMPLATE_SUFFIX'));
	}
	


}