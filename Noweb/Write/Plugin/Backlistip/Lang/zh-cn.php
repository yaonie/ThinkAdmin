<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | IP黑名单插件语言文件
 * +--------------------------------------------------------------------------------------------
 * | IP黑名单插件语言文件包含当前插件所使用的语言
 * +--------------------------------------------------------------------------------------------
 */
return array(
	'BACKLIST_TIP' 				=> '<li>IP黑名单可以限制某些IP访问网站</li><li>双击IP可以对其编辑</li>',
	'ADDIP_TIP_BACKLIST_TIME' 	=> '如果想永久禁止请手动输入0,如果被控件遮住,请按ESC键关闭控件手动输入',
	'THISIP_PERMANENT_BAN_TIP' 	=> '当前IP已被永久禁止',
  	'THISIP__BAN_TIME_TIP'		=> '当前IP已被系统禁止<br />禁止截止时间为:',
	'EDITBACKLISTIP' 			=> '修改了IP黑名单编号:',
    'BACKLISTIP' 				=> 'IP黑名单',
	'BAN_JZTIME' 				=> '禁止截止',
	'PLEASE_INPUT_IP'			=> '请输入IP',
	'IP_IS_IN'					=> 'IP已经存在',
	'IP_IS_OK'					=> 'IP可用',
);