<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |IP黑名单模型
 * +--------------------------------------------------------------------------------------------
 * |IP黑名单表模型
 * +--------------------------------------------------------------------------------------------
 */

namespace Write\Plugin\Backlistip\Model;
class PluginBacklistipModel extends \Common\Model\CommonModel{
	
	public $onlyField = array('ip'); //唯一字段 
	
	public $_filter = array(
		'lockmodule'=>array('serialize','unserialize'), //对值进行数组和字符串的转换
	); //高级模型的字段过滤规则

	/**
	 *
	 * 根据IP地址获取他的锁定信息
	 * @param $ip 获取验证的ip
	 * @return $lock 数组，module 锁定的模块，time -1表示ip不在黑名单中IP通过 	0表示ip已被永久拉黑	返回其他数字表示ip的解禁时间戳
	 * @ 提示:请不要使用S缓存，S缓存如果用户配置缓存为memcache,那么可能会导致用户恶意切换IP导致验证的IP缓存过多导致内存不足;
	 *	 F缓存是文件方式保存,只要硬盘充足即可
	 */
	public function getLock($ip=''){
		if($ip === '') return false;
		$lock = F($this->getModelName().md5($ip),	'',	DATA_PATH.'./'.$this->getModelPrefix().'/'.$this->getModelName() . './verify/');
	
		if($lock === false){  //如果没有缓存数据重新处理
			$info = $this->getInfo($ip, 'Adv', 'Plugin');
			$lock['module'] = $info ? $info['lockmodule'] : array();
			$lock['time'] = $info ? $info['locktime'] : -1;
			F($this->getModelName().md5($ip),	$lock, DATA_PATH.'./'.$this->getModelPrefix().'/'.$this->getModelName() . './verify/');
		}
		return  $lock;
	}
}