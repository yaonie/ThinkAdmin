<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |单点登录插件
 * +--------------------------------------------------------------------------------------------
 * |单点登录插件可以让其他相同账号登陆下线
 * +--------------------------------------------------------------------------------------------
 */
namespace Write\Plugin\Singlelogin\Controller;
use Common\Controller\CommonController;
class Run extends CommonController{

	public $access = array('addloginInfo','singlecheck');//不允许外部访问的操作

	/**
	 * 单点登录 登录信息记录
	 */
	public function addloginInfo($uid){
		//记录登陆时间
		$loginInfo['time'] = microtime(true);  //记录登录时间
		$loginInfo['ip'] = get_client_ip();  //获取IP地址
		$loginInfo['run'] = MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;  //记录登录入口
		F('SingleLogin_'.$uid, $loginInfo, NOWEB_PATH.'./Write/SingleLogin/');  //更新登录时间
		session('SingleLogin', $loginInfo);  //保存登录信息到session

	}

	/**
	 * 单点登录检查
	 */
	public function singlecheck(){
		$loginInfo = F('SingleLogin_'.session(C('USER_INFO').'.id'),'',NOWEB_PATH.'./Write/SingleLogin/');  //获取登录信息缓存
		$sessionLoginInfo = session('SingleLogin');
		if(!$sessionLoginInfo || $loginInfo['time'] !==$sessionLoginInfo['time'] || $loginInfo['ip'] !== $sessionLoginInfo['ip'] || $loginInfo['run'] !== $sessionLoginInfo['run']){
			//注销
			$loginApi = new \Api\Controller\LoginController;
			$loginApi->out(true);
			$this->msg(0,L('SINGLELOGIN_TIP'));
		}
	}

	/**
	 * 配置
	 * @remark 单点登陆插件
	 */
	public function config(){
		$this->msg(0,L('THIS_PLUGIN_NO_CONFIG'));
	}
	
	
}