<?php 
return array (
  'TYPE' => 'plugin',
  'STATUS' => 1,
  'NAME' => '单点登录插件',
  'REMARK' => '单点登录插件可以限制用户只能从一个地方登录，如果在另一个地方登录可以把以前登录的账号挤下线',
  'RULESSQL' => 
  array (
    0 => 
    array (
      'title' => '单点登录插件',
      'rules' => 'Admin-Plugin-Singlelogin',
      'pid' => 9,
      'status' => 0,
      'sort' => 3,
      'remark' => '单点登录插件权限规则',
    ),
    1 => 
    array (
      'title' => '配置',
      'rules' => 'Admin/Extension/usehook/Plugin/Singlelogin/Run/config',
      'pid' => 'key0',
      'status' => 0,
      'sort' => 1,
      'remark' => '配置单点登录插件权限规则',
    ),
  ),
  'RULESID' => 
  array (
    0 => 42,
    1 => 43,
  ),
) 
?>