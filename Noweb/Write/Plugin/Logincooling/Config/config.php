<?php 
return array (
  'TYPE' => 'plugin',
  'STATUS' => 2,
  'LOGIN_ERROR_NUM' => 5,
  'LOGIN_ERROR_MINUTE' => 30,
  'NAME' => '登录冷却插件',
  'REMARK' => '登录冷却插件是用户登录时操作错误次数过多,可以有效的使用户等待一段时间后再继续登录操作。可以有效的防止用户恶意尝试密码，从而提高安全性',
  'RULESSQL' => 
  array (
    0 => 
    array (
      'title' => '登录冷却插件',
      'rules' => 'Admin-Plugin-Logincooling',
      'pid' => 9,
      'status' => 0,
      'sort' => 2,
      'remark' => '登录冷却插件权限规则',
    ),
    1 => 
    array (
      'title' => '配置',
      'rules' => 'Admin/Extension/usehook/Plugin/Logincooling/Run/config',
      'pid' => 'key0',
      'status' => 0,
      'sort' => 1,
      'remark' => '配置登录冷却插件权限规则',
    ),
  ),
  'RULESID' => 
  array (
    0 => 40,
    1 => 41,
  ),
) 
?>