<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | 登陆冷却插件
 * +--------------------------------------------------------------------------------------------
 * | 登陆冷却插件可以限制用户不断的错误登陆。限制后用户需要冷却后才能登陆
 * +--------------------------------------------------------------------------------------------
 */
namespace Write\Plugin\Logincooling\Controller;
use Common\Controller\CommonController;
class Run extends CommonController{
	public $access = array('coolingcheck', 'coolingaddnum'); //不允许外部访问的操作
	/**
	 * 冷却检查
	 * @param $mark 冷却标识  账号  邮箱  手机
	 */
	public function coolingcheck($mark){
		$isErrorNum = S('LoginErrorNum'.$mark.get_client_ip()); //获取当前标识错误次数
		$config = $this->configs;
		if((int)$isErrorNum >= (int)$config['LOGIN_ERROR_NUM']){
			$this->msg(0,L('LOGIN_ERROR_NUM_TIP_A').$config['LOGIN_ERROR_MINUTE'].L('MINUTE').L('LOGIN_ERROR_NUM_TIP_B'));
		}
	}

	/**
	 *
	 * 冷却次数写入
	 * @param $info 用户信息
	 */
	public function coolingaddnum($info){
		$config = $this->configs;
		$num = S('LoginErrorNum'.$info['id'].get_client_ip()) ? (int)S('LoginErrorNum'.$info['id'].get_client_ip()) : 0;
		$num +=1;
		S('LoginErrorNum'.$info['id'].get_client_ip(), $num, (int)$config['LOGIN_ERROR_MINUTE']*60);//保存追加数
		S('LoginErrorNum'.$info['name'].get_client_ip(), $num, (int)$config['LOGIN_ERROR_MINUTE']*60);//保存追加数
		S('LoginErrorNum'.$info['email'].get_client_ip(), $num, (int)$config['LOGIN_ERROR_MINUTE']*60);//保存追加数
		S('LoginErrorNum'.$info['mobilephone'].get_client_ip(), $num, (int)$config['LOGIN_ERROR_MINUTE']*60);//保存追加数
	}

	/**
	 * 配置
	 * @remark 配置登陆冷却插件
	 */
	public function config(){
		$filename = $this->path.'Config/config.php';
		$config = include $filename;
		//提交操作
		if(IS_POST){
			$postConfig = I('post.config');
			if(!is_format_name($postConfig['NAME'])) $this->msg(0,L('NAME_FORMAT_TIP'));
			$config['NAME'] = \Common\Lib\Input::safeHtml($postConfig['NAME']);
			$config['LOGIN_ERROR_MINUTE'] = (int)$postConfig['LOGIN_ERROR_MINUTE'];
			$config['LOGIN_ERROR_NUM'] = (int)$postConfig['LOGIN_ERROR_NUM'];
			$config['REMARK'] = \Common\Lib\Input::safeHtml($postConfig['REMARK']);
			$msg = \Common\Lib\FileUtil::insertFile("<?php \nreturn ".var_export($config,true)." \n?>", $filename);
			$this->msg($msg[0],$msg[1]);
		}
		$this->assign('config',$config);
		$this->display($this->path.'View/'.MODULE_NAME.'/'.__FUNCTION__.C('TMPL_TEMPLATE_SUFFIX'));
	}

}