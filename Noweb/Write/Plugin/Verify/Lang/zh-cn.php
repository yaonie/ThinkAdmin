<?php
// +--------------------------------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +---------------------------------------------------------------------------------------------------------------------

/**
 * +---------------------------------------------------------------------------------------------------------------------
 * | 验证码插件语言文件
 * +---------------------------------------------------------------------------------------------------------------------
 *  验证码插件语言文件
 * +---------------------------------------------------------------------------------------------------------------------
 */
return array(
        'VERIFY_ERROR' => '验证码错误',
	    'IN_MODULE' => '有效模块',
		'MD5_SKEY' => '加密密钥',
		'EXPIRE' => '过期时间',
		'USEZH' => '是否中文验证码',
		'USEIMGBG' => '使用背景图片',
		'FONTSIZE' => '字体大小',
		'USECURVE' => '使用混淆曲线',
		'USENOISE' => '使用杂点',
		'CODESET' => '英文验证字符串',
		'ZHSET' => '中文验证码字符串',
	    'PLEASE_INPUT_SEKEY' => '请输入加密密钥',
		'SEKEY_TIP' => '加密密钥只能由字母组成',
		'VERIFY_TYPE_TIP' => '请输入0或者其他正整数,0为一直启用验证码,其他数字为错误因子,操作错误次数达到因子数才自动启用验证码',
		'XG' => '效果',
	    'RESET' => '验证码成功后是否重置',
		'VERIFY_START_TYPE' => '验证码启用方式',
		'PLEASE_INPUTVERIFY_NUM' => '请输入正整数或0,0为自动判断',
);