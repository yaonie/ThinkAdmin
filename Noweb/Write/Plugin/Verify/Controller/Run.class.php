<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |验证码插件
 * +--------------------------------------------------------------------------------------------
 * |验证码插件默认控制器
 * +--------------------------------------------------------------------------------------------
 */
namespace Write\Plugin\Verify\Controller;
use Common\Controller\CommonController;
class Run extends CommonController{

	public $access = array('isverify','verify','checkverify','unverifyfactor','addverifyfactor'); //不允许外部访问的操作

	/**
	 *
	 * 判断验证码是否启用
	 */
	public function isverify(){
		$config = $this->configs;
		if(!in_array(MODULE_NAME, $config['VERIFY_MODULE'])) return false; //验证码不包含当前模块
		if($config['VERIFY_START_TYPE'] === 0 ) return true; //验证码一直启用
		return (int)session('error_verify_num') >= (int)$config['VERIFY_START_TYPE'] ? true : false;
	}

	/**
	 * 输出验证码
	 */
	public function verify(){
		!$this->isverify() && $this->_empty();
		$verify = new \Write\Plugin\Verify\Lib\Verify($this->configs);
		$verify->entry(I('get.verifycodekey'));
	}

	/**
	 * 检测验证码是否输入正确
	 */
	public function checkverify($value = ''){
		$verify = new  \Write\Plugin\Verify\Lib\Verify($this->configs);
		if(!$verify->check($value['code'], $value['codekey'])){
			return array(0,L('VERIFY_ERROR'));
		}
		return array(true);
	}

	/**
	 * 清除验证码错误因子数
	 */
	public function unverifyfactor(){
		session('error_verify_num',null); //清除验证码错误因子
	}

	/**
	 * 错误因子数累加
	 */
	public function addverifyfactor(){
		$error_verify_num = session('?error_verify_num') ?  session('error_verify_num') : 0;
		session('error_verify_num',$error_verify_num+1);//保存启用验证码因子
	}

	/**
	 * 配置验证码插件
	 * @remark 配置验证码插件
	 */
	public function config(){
		$filename = $this->path.'Config/config.php';
		$config = include $filename;
		//提交操作
		if(IS_POST){
			if($config['STATUS'] === 0) $this->msg(0,L('PLUGIN_NO_INSTALL'));
			$postConfig = I('post.config');
			if(!is_format_name($postConfig['NAME'])) $this->msg(0,L('NAME_FORMAT_TIP'));
			$config['NAME'] = \Common\Lib\Input::safeHtml($postConfig['NAME']);
			$config['VERIFY_CONFIG']['seKey'] = \Common\Lib\Input::safeHtml($postConfig['VERIFY_CONFIG']['seKey']);
			$config['VERIFY_START_TYPE'] = (int)$postConfig['VERIFY_START_TYPE'];
			$config['VERIFY_CONFIG']['expire'] = (int)$postConfig['VERIFY_CONFIG']['expire'];
			$config['VERIFY_CONFIG']['useZh'] = (bool)$postConfig['VERIFY_CONFIG']['useZh'];
			$config['VERIFY_CONFIG']['codeSet'] = \Common\Lib\Input::safeHtml($postConfig['VERIFY_CONFIG']['codeSet']);
			$config['VERIFY_CONFIG']['zhSet'] = \Common\Lib\Input::safeHtml($postConfig['VERIFY_CONFIG']['zhSet']);
			$config['VERIFY_MODULE'] = (array)$postConfig['VERIFY_MODULE'];
			$config['VERIFY_CONFIG']['length'] = (int)$postConfig['VERIFY_CONFIG']['length'];
			$config['VERIFY_CONFIG']['fontSize'] = (int)$postConfig['VERIFY_CONFIG']['fontSize'];
			$config['VERIFY_CONFIG']['imageW'] = (int)$postConfig['VERIFY_CONFIG']['imageW'];
			$config['VERIFY_CONFIG']['useImgBg'] = (bool)$postConfig['VERIFY_CONFIG']['useImgBg'];
			$config['VERIFY_CONFIG']['useCurve'] = (bool)$postConfig['VERIFY_CONFIG']['useCurve'];
			$config['VERIFY_CONFIG']['useNoise'] = (bool)$postConfig['VERIFY_CONFIG']['useNoise'];
			$config['VERIFY_CONFIG']['reset'] = (bool)$postConfig['VERIFY_CONFIG']['reset'];
			$config['REMARK'] = \Common\Lib\Input::safeHtml($postConfig['REMARK']);
			$msg = \Common\Lib\FileUtil::insertFile("<?php \nreturn ".var_export($config,true)." \n?>", $filename);
			$this->msg($msg[0],$msg[1]);
		}
		$path = scandir(APP_PATH);
		$this->assign('module_list',$path);
		$this->assign('config',$config);
		$this->display($this->path.'View/'.MODULE_NAME.'/'.__FUNCTION__.C('TMPL_TEMPLATE_SUFFIX'));
	}

}