<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |钩子类
 * +--------------------------------------------------------------------------------------------
 * |钩子常用操作
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Lib;
class Hook{

	public $name; //钩子名

	public $type; //钩子类型

	public $path;

	public $lang; //钩子语言

	/**
	 * 初始化构造函数
	 */
	public function __construct($hookType='',$hookName=''){

		$this->name = $hookName;

		$this->type = $hookType;

		$this->path = NOWEB_PATH."Write/$hookType/".$hookName.'/';

		//注册钩子语言
		file_exists($this->path.'./Lang/'.LANG_SET.'.php') && L(include $this->path.'./Lang/'.LANG_SET.'.php');

	}

	/**
	 * 钩子默认调用类
	 */
	public function call($type='', $name='', $action='', $controller='Run', $params='', $status = true, $access = false){
		if(empty($type) || empty($name) || empty($action)) return null;
		if(!$this->isFormat($type,$name)) return null;
		$obj = $this->getClass($type, $name, $action, $controller='Run', $params, $status, $access); //取得实例

		//状态监测
		if($status){
			if(empty($config = $this->getConfig($type, $name))) return null;
			if((int)$config['STATUS'] !==2) return null;
		}

		//获取访问规则
		if($access){ //有外部权限的访问方式
			$access = $obj->access;
			if(in_array($action, $access)) return null;
		}
		if(method_exists($obj, $action)){
			return $obj->$action($params); //触发
		}else{
			return null;
		}
	}

	/**
	 *
	 * 取得配置
	 * @param $type  钩子类型
	 * @param $name 名称
	 */
	public function getConfig($type='', $name=''){
		if(empty($type) ) $type = $this->type;
		if(empty($name) ) $name = $this->name;
		return file_exists($this->path.'./Config/config.php') ? include $this->path.'./Config/config.php' : array();
	}

	/**
	 *
	 * 检查格式是否正确.
	 */
	public function isFormat($type='', $name=''){
		if(empty($type) ) $type = $this->type;
		if(empty($name) ) $name = $this->name;
		$config = $this->getConfig($type,$name);
		if(empty($config)) return false; //必须包含配置
		if(ucfirst(strtolower($type))  !== ucfirst(strtolower($config['TYPE']))) return false;
		return true;
	}

	/**
	 *
	 * 获取钩子控制器实例
	 * @param $controller 控制器名 默认Run
	 */
	public function getClass($type='', $name='', $action='', $controller='Run', $params='', $status = true, $access = false){
		$controllerPath = $this->path.'./'.C('DEFAULT_C_LAYER').'/'.ucfirst(strtolower($controller)).EXT;
		if(!file_exists($controllerPath)) return null;
		$classname = '\\Write\\'.$this->type.'\\'.$this->name.'\\'.C('DEFAULT_C_LAYER').'\\'.ucfirst(strtolower($controller));
		$obj = new $classname;
		$obj->configs = $this->getConfig($type, $name);
		$obj->path = $this->path;
		return $obj;
	}


}