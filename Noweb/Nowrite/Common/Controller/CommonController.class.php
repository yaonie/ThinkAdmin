<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | 项目公共控制器
 * +--------------------------------------------------------------------------------------------
 * | 项目公共控制器是定义一些公共操作方法，这些方法一般只有继承才能调用，不会被用户访问，所以建议方法属
 * | 性设置为protected属性
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Controller;
use Think\Controller;
class CommonController extends Controller{

	/**
	 *
	 * 404错误页面
	 */
	protected function _empty(){
		IS_AJAX && $this->msg(0, L('PAGE_EMPTY_TIP'));	//AJAX请求方式采用msg方法判断提示输出
		IS_POST && (I('post.posttype') =='uploadify') &&  $this->msg(0, L('PAGE_EMPTY_TIP'));	//POST请求方式采用msg方法判断提示输出
		$this->assign('title', L('ERROR_404'));
		$this->display('Public:404'); //输出页面
		exit; //必须结束。不然会重复输出页面
	}
	/**
	 *
	 * 全局提示方法
	 * @param $status 状态 失败0 成功1
	 * @param $message 提示信息
	 * @param $title 提示标题
	 * @param $back 跳转地址, 如果为'false'表示不跳转,注意是字符串false
	 * @param $data  需要返回的数据信息
	 */
	protected function msg($status = 0, $message = '', $title='', $back = 'javascript:history.back(-1);', $data = null){
		//默认值设置
		empty($title) && $title = L('TIP_INFO');
		if($back !=='false'){
			isset($_SERVER['HTTP_REFERER']) && $back = $_SERVER['HTTP_REFERER'];
			(isset($_GET['urlreturn']) && !empty($_GET['urlreturn'])) &&  $back = $_GET['urlreturn'];
		}
		if(IS_POST){
			//状态验证码处理
			$status ? HOOK('Plugin','Verify','unVerifyFactor') : HOOK('Plugin','Verify','addVerifyFactor');
			//uploadify上传返回提示信息格式
			if(I('post.posttype') =='uploadify'){
				echo json_encode(array('status'=>$status,'msg'=>$message,'data'=>$data));
				exit;
			}
		}
		if(IS_AJAX){ //ajax请求
			$retuanData['isverify'] = HOOK('Plugin','Verify','isverify'); //返回是否启用验证码
			!empty($data) && $retuanData['data'] = $data;  //如果存在数据设置数据
			exit($this->ajaxReturn(array('status'=>$status, 'title'=>$title, 'message'=>$message, 'back'=>$back, 'data'=>$retuanData))); //ajax方式的请求返回方式
		}
		$this->assign('waitSecond', C('WAIT_SECOND'));
		$status ? $this->success($message, $back) : $this->error($message, $back);
	}


	/**
	 * 获取登录信息
	 */
	protected function userLoginInfo($uid = false){
		$uidOldVal  = $uid;
		if(!$uid){
			$userInfo = session(C('USER_INFO'));
			$uid = $userInfo['id'];
			$userLoginInfo = session('user_login_info');
		}else{
			$userLoginInfo = false;
		}
		if(!is_array($userLoginInfo)){
			$user = new \Common\Model\SystemUserModel;
			$where['id'] = array('eq',	$uid);
			$userLoginInfo = $user->where($where)->field('loginnum,lastloginip,lastlogintime,theloginip,thelogintime')->find();
			!$uidOldVal && session('user_login_info',$userLoginInfo);
		}
		return $userLoginInfo;
	}

	/**
	 * 条件初始化，主要用于按照条件搜索时返回公共的条件
	 */
	protected function get_where(){
		$where = array();  //初始化条件定义
		//时间条件处理，包含创建时间与更新时间
		$startTime = I('get.starttime');
		$endTime = I('get.endtime');
		if($startTime || $endTime){
			if(I('get.timetype')){  //按照更新时间
				!empty($startTime) && empty($endTime) && $where[I('get.timetype')]=array("egt",strtotime($startTime)); //定义起始条件
				!empty($endTime)&&empty($startTime) && $where[I('get.timetype')]=array("elt",strtotime($endTime));  //定义结束条件
				!empty($startTime) && !empty($endTime) && $where[I('get.timetype')]=strtotime($startTime)<strtotime($endTime) ? array(array('egt',strtotime($startTime)),array('elt',strtotime($endTime))) : array(array('egt',strtotime($endTime)),array('elt',strtotime($startTime)));  //同时出现
			}else{  //按照创建时间
				!empty($startTime)&&empty($endTime) && $where['createtime']=array("egt",strtotime($startTime)); //定义起始条件
				!empty($endTime)&&empty($startTime) && $where['createtime']=array("elt",strtotime($endTime));  //定义结束条件
				!empty($startTime)&&!empty($endTime) && $where['createtime']=strtotime($startTime)<strtotime($endTime) ? array(array('egt',strtotime($startTime)),array('elt',strtotime($endTime))) : array(array('egt',strtotime($endTime)),array('elt',strtotime($startTime)));  //同时出现
			}
		}
		//搜索关键词处理
		if(!empty($_GET['keyword'])){
			$where[I('get.types')]=array('like','%'.I('get.keyword','').'%');
		}
		return $where;  //返回条件数组
	}

}