<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | ThinkAdmin应用初始化
 * +--------------------------------------------------------------------------------------------
 * | 执行初始化，配置初始化
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Behavior;
class ThinkAdminInitBehavior{

	//行为执行入口
	public function run(&$param){
		$this->regConfig(); //注册全局配置
	}

	/**
	 *
	 * 注册全局配置
	 */
	private function regConfig(){
		$mConfig = new \Common\Model\SystemConfigModel;
		$config = $mConfig->getInfo('Common','Adv');
		$config = $config['config'];
		$config['TMPL_PARSE_STRING']['__SKINPATH__'] = str_replace('__ROOT__', __ROOT__, $config['SKIN_PATH']);//相关常量替换
		$config['SESSION_PATH'] = str_replace('__NOWEB_PATH__', NOWEB_PATH, $config['SESSION_PATH']);//相关常量替换
		C($config); //注册配置
		date_default_timezone_set(C('DEFAULT_TIMEZONE')); //设置时区
	}
}