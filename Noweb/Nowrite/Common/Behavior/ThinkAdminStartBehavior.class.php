<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | 项目初始化控制行为
 * +--------------------------------------------------------------------------------------------
 * | 项目初始化控制行为是对整个项目初始化控制，进行当面模块配置注册,语言注册,安装,等公共初始化操作执行
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Behavior;
use Common\Controller\CommonController;
class ThinkAdminStartBehavior extends CommonController{
	
	/**
	 *
	 * 行为扩展的执行入口
	 * @param  $params 接收参数
	 */
	public function run(&$params){
		$this->regConfig(); //注册当前模块配置
		$this->regLang(); //注册语言
		//开始解析静态缓存
		\Think\Hook::add('ReadHtmlCacheBehavior','Behavior\ReadHtmlCacheBehavior');
		\Think\Hook::listen('ReadHtmlCacheBehavior');
		//开始解析静态缓存End

		MODULE_NAME !== 'Install' && !file_exists(NOWEB_PATH.'./Write/Install/install.lock')	&&	redirect(URL(C('INSTALL_ENTER'))); //程序安装检测
		MODULE_NAME === 'Install' && file_exists(WRITE_PATH.'./Data/Install/install.lock') && $this->_empty(); //程序已经安装访问安装模块抛出404错误
		C('MODULE_CLOSE') && $this->msg(0,L('MODULE_CLOSE_TIP'), '', 'false'); //检测模块是否正常开放
		session(array('name'=>session_name(),'prefix'=>C('SESSION_PREFIX'),'expire'=>C('SESSION_EXPIRE'),'path'=>C('SESSION_PATH')));  //启动session
		C('SESSION_AUTO_START',false); //防止重复启动session

		HOOK('Plugin','Backlistip','verifybacklistip'); //IP黑名单钩子检测IP黑名单
		
		!session('?'.C('USER_INFO')) && $this->startUserInfo();//初始化用户信息
		C('IS_LOGIN') && A('Api/Login')->islogin('back'); //判断是否需要登陆
		A('Api/Login')->islogin('return') && HOOK('Plugin','Singlelogin','singlecheck'); //单点登录检查钩子插件

		//权限检查
		if(C('IS_AUTH')){	//开启了权限认证
			if(!in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME, C('NO_AUTH_RULES'))){	//不在放行的规则中
				!RULES_AUTH(array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME)) && $this->msg(0,L('NO_ACCESS'),'','false');
			}
		}

	}

	/**
	 *
	 * 注册模块配置
	 */
	private function regConfig(){
		$mSystemConfig = new \Common\Model\SystemConfigModel; //获取模型实例
		$config = $mSystemConfig->getInfo(MODULE_NAME,'Adv'); //获取公共配置
		$config = $config['config'];
		C($config); //注册配置
	}

	/**
	 *
	 * 注册语言
	 */
	private function regLang(){
		$mSystemLang = new \Common\Model\SystemLangModel; //获取模型实例
		$lang = $mSystemLang->getLang(MODULE_NAME); //获取语言
		L($lang); //注册配置
	}

	/**
	 *
	 * 初始化用户信息
	 */
	private function startUserInfo(){
		$userInfo['name']	=	L('VISITOR');//保存用户名,定位游客
		$role	=	new \Common\Model\SystemRoleModel;
		$roleInfo	=	$role->getInfo(C('VISITORS_ROLE_ID'));//获取游客角色信息
		!$roleInfo	&&	$this->msg(0,L('VISITORS_ROLE_ID_SET_ERROR'),'','false');//未登录用户默认角色组设置错误
		$userInfo['role_id']	=	$roleInfo['id'];
		$userInfo['role_name']	=	$roleInfo['name'];
		session(C('USER_INFO'), $userInfo); //保存用户信息
	}

}