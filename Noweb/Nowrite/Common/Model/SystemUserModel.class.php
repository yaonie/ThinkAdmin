<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |
 * +--------------------------------------------------------------------------------------------
 * |
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Model;
class SystemUserModel extends CommonModel{
	public $onlyField = array('name','email','mobilephone'); //唯一字段
	//关联定义
	protected $_link = array(
        'SystemUserRole'=> array(   //关联用户角色关系表
             'mapping_type' => 1,
             'class_name'=>'SystemUserRole',
             'mapping_name'=>'role',
             'foreign_key'=>'user_id',
	         'mapping_fields'=>'role_id',
			 'as_fields'=>'role_id',
	),
		'PluginLog'=> array(  //关联用户日志
           'mapping_type'=>3,
           'class_name'=>'PluginLog',
           'foreign_key'=>'user_id',
           'mapping_name'=>'Log',
	),
	);

	/**
	 * 根据用户id获取用户快捷菜单
	 * @$userId 用户id
	 * @$menuTypeId 菜单所属类型id
	 * @$role_id 角色id
	 */
	public function shortcutMenu($userId = '', $menuTypeId = ''){
		if(empty($userId) || empty($menuTypeId)) return false;
		$menu = F($this->getModelName().'_Shortcutmenu_'.$userId, '', DATA_PATH.'./'.$this->getModelPrefix().'/'.$this->getModelName().'/Shortcutmenu/');
		if(!isset($menu[$menuTypeId])){  //如果不存在当前用户的所属模块快捷菜单尝试缓存获取
			
			$mMenu = new \Common\Model\SystemMenuModel;
			$menuField = $mMenu->viewFields('','menu_');
			$menuField['SystemMenu']['_on'] = 'SystemMenu.id = SystemUserShortcutmenu.menu_id';
			
			$viewFields = $this->viewFields(M('SystemUserShortcutmenu'));  //设置关联属性
			$viewFields = array_merge($viewFields,$menuField);

			$where['menu_status'] = array('eq', 1);  //状态为正常的
			$where['menutype_id'] = array('eq', $menuTypeId);  //查询指定模块菜单的
			$where['user_id'] = array('eq', $userId);  //查询当前用户的菜单
			$userView = M('\Common\Model\ViewModel:SystemUser');  //动态继承视图模型
			$userView->setProperty("viewFields",$viewFields);  //设置属性
			$list = $userView->where($where)->order('menu_sort asc')->select();
			$menu[$menuTypeId] = $list  ?  $list : array();
			F($this->getModelName().'_Shortcutmenu_'.$userId, $menu, DATA_PATH.'./'.$this->getModelPrefix().'/'.$this->getModelName().'/Shortcutmenu/');
			
		}
		return $menu[$menuTypeId];
	}
}