<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | 权限规则模型
 * +--------------------------------------------------------------------------------------------
 * | 权限规则模型
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Model;
class SystemRulesModel extends CommonModel{

	//自动验证
	protected $_validate = array(

	array('title','1,16','{%NAME_LENGTH_ERROR_TIP}',2,'length'), // 长度判断

	array('title','require','{%NAME_NO_EMPTY_TIP}'), //名称不能为空

	array('title','is_format_name','{%NAME_FORMAT_TIP}',0,'function'), //名称正则检查格式

	array('rules','require','{%RULES_NO_EMPTY}'), //规则不能为空

	);

	//自动完成
	protected $_auto = array (

	array('remark','get_safe_html',3,'function'), // 对remark字段在新增会更新的时候进行安全过滤

	array('remark','',2,'ignore'), // 对remark字段修改的时候如果留空忽略

	);

	//关联定义
	protected $_link = array(
	 
		'SystemRoleRules'=> array( 
			'mapping_type'=>3,
			'class_name'=>'SystemRoleRules',
			'foreign_key'=>'rules_id',
			'mapping_name'=>'SystemRoleRules',
	),

	);

	/**
	 * 删除缓存还需执行的操作
	 */
	public function _dalete_cache(){
		//更新所有角色的菜单
		$mRole = new \Common\Model\SystemRoleModel;
		\Common\Lib\FileUtil::unlinkDir(DATA_PATH. './'.$mRole->getModelPrefix().'/' . $mRole->getModelName().'/Rules/'); //删除角色菜单缓存
	}
}