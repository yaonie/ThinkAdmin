<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |菜单模型
 * +--------------------------------------------------------------------------------------------
 * |菜单表模型
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Model;
class SystemMenuModel extends CommonModel{
	public $onlyField = array(); //唯一字段

	/**
	 * 删除缓存还需执行的操作
	 */
	public function _dalete_cache(){
		//更新所有角色的菜单
		$mRole = new \Common\Model\SystemRoleModel;
		\Common\Lib\FileUtil::unlinkDir(DATA_PATH. './'.$mRole->getModelPrefix().'/' . $mRole->getModelName().'/Menu/'); //删除角色菜单缓存
		
		//更新用户快捷菜单
		$mUser = new \Common\Model\SystemUserModel;
		\Common\Lib\FileUtil::unlinkDir(DATA_PATH.'./'.$mUser->getModelPrefix().'/'.$mUser->getModelName().'/Shortcutmenu/');
		
	}
}