<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | 语言模型
 * +--------------------------------------------------------------------------------------------
 * | 语言表模型，封装了一些常用操作方法
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Model;
class SystemLangModel extends CommonModel{

	public $onlyField = array(); //唯一字段
	
	public $_filter = array(
    	'lang'=>array('strtoupper', ''),	//对语言键名强制大写
	); //高级模型的字段过滤规则
	
	/**
	 *
	 * 获取对应模块的语言
	 * @param $module 模块
	 * @return array() 语言参数数组
	 */
	public function getLang($module=''){
		$lang = F($module.'_lang_'.LANG_SET,'', DATA_PATH.'./'.$this->getModelPrefix().'/'.$this->getModelName().'./lang/'.LANG_SET.'/'); //获取缓存
		if($lang === false){ //缓存不存在
			$lang = array(); //定义一个空数组语言
			//从数据库查询配置
			$where['module'] = array(array('eq',$module),array('eq','Common'),'or');
			$where['type'] = array('eq',LANG_SET);
			$list = $this->where($where)->field('lang,value')->select();
			//如果存在查询数据对数据处理
			if($list !== null){
				foreach ($list as $k=>$v){
					$langv  = str_replace('__APP__', __APP__, str_replace('__ROOT__', __ROOT__, $v['value']));
					$lang[$v['lang']] = $langv;
				}
			}
			F($module.'_lang_'.LANG_SET, $lang, DATA_PATH.'./'.$this->getModelPrefix().'/'.$this->getModelName().'./lang/'.LANG_SET.'/');
		}
		return $lang;
	}

}