<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | 配置模型
 * +--------------------------------------------------------------------------------------------
 * | 配置表模型类
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Model;
class SystemConfigModel extends CommonModel{

	public $onlyField = array('type'); //唯一字段

	public $_filter = array(
    	'config'=>array('serialize', 'unserialize'), //对值进行数组和字符串的转换
	); //高级模型的字段过滤规则

	/**
	 *
	 * 根据唯一字段更新数据
	 * @param $onlyField 唯一字段内容
	 * @param $val 内容
	 */
	public function saveConfig($onlyField = '',  $data = array()){
		if(empty($onlyField)) return false;
		$whereStr =$this->getPk();
		if(isset($this->onlyField) && is_array($this->onlyField)){
			$whereStr.= '|'. implode('|', $this->onlyField);
		}
		$where[$whereStr] = array('eq', $onlyField);
		if($this->getAdvModel()->where($where)->save($data) !==false){
			$this->deleteCache($onlyField);
			return true;
		}else{
			return false;
		}
	}
}