<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |项目公共模型
 * +--------------------------------------------------------------------------------------------
 * |实现模型的公共方法
 * +--------------------------------------------------------------------------------------------
 */
namespace Common\Model;
use Think\Model;
class CommonModel extends Model{

	/**
	 *
	 * 根据唯一字段获取信息
	 * @param $onlyField 唯一字段
	 * @param $type 调用方式
	 * @param $modelType 模型类型，系统还是很插件
	 */
	public function getInfo($onlyField='id', $type='default'){
		$info = F($this->getModelName().'_info_'.md5($onlyField), '', DATA_PATH. './'.$this->getModelPrefix().'/' . $this->getModelName().'/Info/');
		if(!isset($info[$type])){ //缓存不存在
			switch ($type){
				case 'Adv': //高级模型
					$obj = $this->getAdvModel();
					break;
				case 'View': //高级模型
					$obj = $this->getViewModel();
					break;
				case 'Relation': //高级模型
					$obj = $this->getRelationModel();
					break;
				case 'default'://默认
					$obj = $this;
					break;
			}
			$whereStr = $type ==='View' ? $this->getModelName().'.'.$this->getPk():$this->getPk();
			if(isset($this->onlyField) && is_array($this->onlyField)){
				$whereStr.= ($type ==='View') ? ('|'.$this->getModelName().'.'. implode('|'.$this->getModelName().'.', $this->onlyField)) : ('|'. implode('|', $this->onlyField));
			}
			$where[$whereStr] = array('eq', $onlyField);
			$info[$type] = $obj->where($where)->find();
			if($info[$type]){
				(isset($info[$type][$this->pk]) && !empty($info[$type][$this->pk])) && F($this->getModelName().'_info_'.md5($info[$type][$this->pk]), $info, DATA_PATH. './'.$this->getModelPrefix().'/' . $this->getModelName().'/Info/');
				foreach ($this->onlyField as $k=>$v){
					(isset($info[$type][$v]) && !empty($info[$type][$v])) && F($this->getModelName().'_info_'.md5($info[$type][$v]), $info, DATA_PATH. './'.$this->getModelPrefix().'/' . $this->getModelName().'/Info/');
				}
			}
		}
		return $info[$type];
	}

	/**
	 *
	 * 获取模型前缀
	 * @param  $model 模型名
	 */
	public function getModelPrefix($model = false){
		$model = $model ? $model : $this->getModelName();
		$model = parse_name($model);
		$model = explode('_', $model);
		return ucfirst($model[0]);
	}

	/**
	 *
	 * 取得视图模型实列
	 */
	public function getViewModel(){
		$obj = M('\Common\Model\ViewModel:'.$this->getModelName());
		$obj->setProperty("viewFields",$this->viewFields);
		return $obj;
	}

	/**
	 *
	 * 取得高级模型实列
	 */
	public function getAdvModel(){
		$obj = M('\Common\Model\AdvModel:'.$this->getModelName());
		$obj->setProperty("_filter",$this->_filter);
		$obj->setProperty("serializeField",$this->serializeField);
		$obj->setProperty("blobFields",$this->blobFields);
		$obj->setProperty("readonlyField",$this->readonlyField);
		$obj->setProperty("partition",$this->partition);
		return $obj;
	}

	/**
	 *
	 * 取得关联模型实列
	 */
	public function getRelationModel(){
		$obj = M('\Common\Model\RelationModel:'.$this->getModelName());
		$obj->setProperty("_link",$this->_link);
		return $obj;
	}

	/**
	 * 视图模型设置默认字段
	 */
	public function viewFields($obj = '', $asPrefix = ''){
		empty($obj) && $obj = $this;
		$fieldList = $obj->fields;
		$fieldList  = array_keys($fieldList['_type']);
		if(!empty($asPrefix)){
			foreach ($fieldList as $k=>$v){
				unset($fieldList[$k]);
				$fieldList[$v] = $asPrefix.$v;
			}
		}
		$field[$obj->name] = $fieldList;
		return $field;
	}

	/**
	 *
	 * 清空缓存解析
	 * @param $mark	标识
	 */
	public function deleteCache($mark = ''){
		if(empty($mark)){  //没有传入标识表示清空所有缓存
			\Common\Lib\FileUtil::unlinkDir(DATA_PATH. './'.$this->getModelPrefix().'/' . $this->getModelName().'/'); //直接删除对应缓存目录
			if(method_exists($this,'_dalete_cache')){
				$this->_dalete_cache();
			}
		}elseif(is_numeric($mark)){  //如果$id是int类型，根据id编删除对应缓存缓存
			$this->_cachedelete($mark);
		}elseif(is_array($mark)){  //如果是数组
			foreach ($mark as $k=>$v){
				$this->_cachedelete($v);
			}
		}else{  //可能是字符串处理成数组形式删除,字符串格式 1,2,3,4...
			$arr = explode(',', $mark);
			foreach ($arr as $k=>$v){
				$this->_cachedelete($v);
			}
		}
	}

	/**
	 * 删除缓存
	 * @param $mark  标识,表唯一字段
	 */
	public function _cacheDelete($mark){
		//删除前先获取信息，方便删除所有唯一字段缓存
		$info = $this->getInfo($mark);
		//删除自身信息缓存
		if($info){
			F($this->getModelName().'_info_'.md5($info[$this->pk]), null, DATA_PATH. './'.$this->getModelPrefix().'/' . $this->getModelName().'./info/');
			foreach ($this->onlyField as $k=>$v){
				F($this->getModelName().'_info_'.md5($info[$v]), null, DATA_PATH. './'.$this->getModelPrefix().'/' . $this->getModelName().'./info/');
			}
		}
		if(method_exists($this,'_dalete_cache')){
			$this->_dalete_cache();
		}
	}
}