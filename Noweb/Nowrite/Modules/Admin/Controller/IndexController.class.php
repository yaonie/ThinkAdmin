<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | Admin模块默认控制器
 * +--------------------------------------------------------------------------------------------
 * | 加载后台界面框架等
 * +--------------------------------------------------------------------------------------------
 */
namespace Admin\Controller;
class IndexController extends AdminController{
	/**
	 *
	 * 后台框架
	 * @remark 后台框架页面，登陆后台
	 */
	public function index(){

		if(I('get.type') ==='main'){ //输出欢迎页面
			$userInfo = session(C('USER_INFO'));
			$this->assign('team', get_contents(C('UPDATE_HTTP').'/down/team.html',true));
			$this->assign('userlogininfo', $this->userLoginInfo());
			$this->assign('user',$userInfo)->assign('sysinfo', get_sysinfo())->display('main');exit;
		}
		$this->assign('user',session(C('USER_INFO')))->display();
	}

	/**
	 *
	 * 搜索菜单
	 * @remark 后台用户搜索菜单
	 */
	public function searchmenu(){
		$keyword =  I('get.keyword');
		$where['name']  = array('like', '%'.$keyword.'%');
		$where['remark']  = array('like', '%'.$keyword.'%');
		$where['_logic'] = 'or';
		$map['_complex'] = $where;
		$map['url']  = array('neq', '');
		$menu = M('SystemMenu');
		$map['status'] = array('eq',1);
		$map['menutype_id'] = array('eq',	1);
		$roleid = session(C('USER_INFO').'.role_id');
		$allMenu = $menu->where($map)->field('id as menu_id, name as menu_name,url as menu_url, color as menu_color, bold as menu_bold, pid as menu_pid, sort as menu_sort, status as menu_status, fun as menu_fun, remark as menu_remark,  target as menu_target, rules as menu_rules,rulestype as menu_rulestype, menutype_id')->select();
		$rolemenu = array();
		if($roleid == C('ADMIN_ROLE_ID') || !C('IS_AUTH')){  //如果是超级管理员或者系统没有要求权限直接返回所有菜单
			$rolemenu =  $allMenu;
		}else{  //角色菜单处理
			foreach($allMenu	as $k=>$v){
				if(empty($v['menu_rules'])){  //如果菜单不存在规则表示所有角色通用
					$rolemenu[] = $v;
				}else{	//检测用户是否存在该菜单的规则
					if(RULES_AUTH(explode(',', $v['menu_rules']),$v['menu_rulestype'],$roleid)){
						$rolemenu[] = $v;
					}
				}
			}
		}
		foreach ($rolemenu as $k=>$v){
			($v['menu_fun'] == 1 && !empty($v['menu_url']) ) &&  $rolemenu[$k]['menu_url'] = MENUURL($rolemenu[$k]['menu_url']);
		}
		$this->assign('menu',$rolemenu)->display();
	}

	/**
	 * 快捷菜单设置
	 * @remark 后台用户设置快捷菜单
	 */
	public function shortcutmenu(){
		$userInfo =  session(C('USER_INFO'));  //获取session
		if(IS_POST){
			$userShortcutMenu = M('SystemUserShortcutmenu');
			$userShortcutMenu->startTrans(); //启动事务
			$userId = $userInfo['id'];  //获取用户ID
			$roleId = $userInfo['role_id'];  //获取角色
			$map['user_id'] = array('eq', $userId);  //当前用户
			//清空旧数据并更新缓存
			$userShortcutMenu->where($map)->delete();//先清空
			$user = new \Common\Model\SystemUserModel;
			F($user->getModelName().'_Shortcutmenu_'.$userId, null, DATA_PATH.'./'.$user->getModelPrefix().'/'.$user->getModelName().'/Shortcutmenu/');//清空快捷菜单缓存
			$menuId = I('post.menu_id');
			if(is_array($menuId)){
				$data = array();
				foreach ($menuId as $k=>$v){
					$data[$k]['menu_id']=$v;
					$data[$k]['user_id'] = $userId;
				}
				if($userShortcutMenu->addAll($data)){
					$userShortcutMenu->commit();	//提交事务
					$this->msg(1,L('SET_SUCCESS'));
				}else{
					$userShortcutMenu->rollback();	//事务回滚
					$this->msg(0,L('SET_ERROR'));
				}
			}else{
				$userShortcutMenu->commit();	//提交事务
				$this->msg(1,L('SET_SUCCESS'));
			}
		}
		$userModel = new \Common\Model\SystemUserModel;
		$userShortMenu = $userModel->shortcutMenu($userInfo['id'], 1);  //获取到用户拥有的当前模块的快捷菜单
		$roleId = $userInfo['role_id']; //获取角色id
		//获取角色菜单
		$role = new \Common\Model\SystemRoleModel;
		$menu = $menu = $role->getRoleMenu($roleId,1,'level');
		if($menu){
			$inMenuId = array();
			foreach ($userShortMenu as $k=>$v){
				$inMenuId[] = $v['menu_id'].'_menu';
			}
			$this->assign('inMenuId',$inMenuId);
		}
		$this->assign('menu',$menu)->display();
	}
}