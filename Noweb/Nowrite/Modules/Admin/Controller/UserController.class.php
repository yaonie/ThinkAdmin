<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |面板控制器
 * +--------------------------------------------------------------------------------------------
 * |修改密码，修改资料等用户操作
 * +--------------------------------------------------------------------------------------------
 */
namespace Admin\Controller;
class UserController extends AdminController{
	/**
	 *
	 * 修改密码
	 * @remark 用户修改密码
	 */
	public function changepassword(){
		$userInfo = session(C('USER_INFO')); //获取用户信息
		if(IS_POST){  //提交请求
			//旧密码检查
			$oldPassword = I('post.oldpassword',I('get.oldpassword'));
			$oldFormatPassword = is_format_password($oldPassword); //格式检查
			if(!$oldFormatPassword[0]) $this->msg(0,L('OLDPASSWORD_FORMAT_ERROR')); //格式错误
			$mUser = new \Common\Model\SystemUserModel;
			$info = $mUser->getInfo($userInfo['id']); //获取用户信息
			if($info['password'] !== get_md5_rules($oldPassword)) $this->msg(0,L('OLD_PASSWORD_ERROR'));
			I('post.posttype')  === 'check' && $this->msg(1,'',L('INPUT_RIGHT')); //外部ajax检测

			//新密码格式检查
			$password = I('post.password');
			$passwordFormat = is_format_password($password); //格式检查
			if(!$passwordFormat[0]) $this->msg(0,L('NEWPASSWORD_FORMAT_ERROR')); //格式错误

			//重复密码检查
			if(I('post.pwdconfirm') !== $password)  $this->msg(0,L('PWDCONFIRM_TIP')); //二次输入不一致
			//开始更新密码
			$user = new \Common\Model\SystemUserModel;
			$where['id'] = array('eq', $userInfo['id']);
			if($user->where($where)->setField('password',get_md5_rules($password)) !== false){
				$user->deleteCache($userInfo['id']);
				$this->msg(1,L('CHANGE_SUCCESS'));
			}else{
				$this->msg(0,L('CHANGE_ERROR'));
			}
		}
		$this->assign('userlogininfo',$this->userLoginInfo());
		$this->assign('user', $userInfo)->display();
	}
}