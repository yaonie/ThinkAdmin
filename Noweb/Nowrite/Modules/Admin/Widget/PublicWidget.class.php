<?php
//*
//*+--------------------------------------------------------------------------------------------------------------
//* | TPAPP 企业内容管理系统
//*+--------------------------------------------------------------------------------------------------------------
//* | Copyright (c) 2013-2014 http://tpapp.cn All rights reserved.
//*+--------------------------------------------------------------------------------------------------------------
//* | Author: 韩仔 <admin@tpapp.cn>
//*+--------------------------------------------------------------------------------------------------------------
//*

/**
 *
 * 公共挂件
 * @author Administrator
 *
 */
namespace Admin\Widget;
use Common\Widget\CommonWidget;
class PublicWidget extends CommonWidget{

	/**
	 *
	 * 输出菜单json
	 */
	public function menu(){
		$userInfo =  session(C('USER_INFO'));  //获取session
		$roleId = $userInfo['role_id']; //获取角色id
		$role = new \Common\Model\SystemRoleModel;
		$menu = $role->getRoleMenu($roleId,1,'level');
		$isUseRules = RULES_AUTH(array('Admin/Index/shortcutmenu')); //检测是否拥有快捷菜单权限	
		$isUseRules && $shortMenu['0_menu'] = array(  //构建一个 菜单
		        'menu_name'=>L('USED'),  //常用
		        'menu_id'=>'0_menu',
		        'menu_pid'=>0,
		        'menu_url'=>'',
		        'menu_items'=>array(
					 '1_menu'=>array(
					      'menu_name'=>L('SHORTCUTMENU'),  //快捷菜单
		                  'menu_id'=>'1_menu',
						  'menu_pid'=>'0',
		                  'menu_url'=>U('Admin/Index/shortcutmenu'),
		                  'menu_items'=>'',
						),	
				),
		);
		
		//获取用户快捷菜单
		$userView = new \Common\Model\SystemUserModel;
		$userShortMenu = array_filter(menutree($userView->shortcutMenu($userInfo['id'], 1)));
		if(is_array($userShortMenu)){
			foreach ($userShortMenu as $k=>$v){
				if(is_array($v['menu_items'])){
					$shortMenu['0_menu']['menu_items'] = array_merge($shortMenu['0_menu']['menu_items'],$v['menu_items']);
				}
			}
		}
		
		$menu= $isUseRules ? array_merge($shortMenu,$menu) : $menu;
		
		echo json_encode($menu);
	}

}