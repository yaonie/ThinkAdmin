<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * | 登陆控制器
 * +--------------------------------------------------------------------------------------------
 * | 登陆接口控制器
 * +--------------------------------------------------------------------------------------------
 */
namespace Api\Controller;
class LoginController extends ApiController{

	/**
	 *
	 * 判断是否登陆
	 * @param $type 没有登陆的处理方式  back跳转至登陆网关
	 */
	public function islogin($type = ''){
		$islogin = false; //是否登陆判断变量
		$roleId = session(C('USER_INFO').'.role_id');	//获取角色id
		if($roleId){
			if((int)$roleId !== C('VISITORS_ROLE_ID')){ //如果角色id不是游客角色id表示已经登陆
				$islogin = true;
			}
		}
		switch ($type){
			case 'back':
				!$islogin && redirect(URL('Api/Login/index',array('urlreturn'=>__SELF__))); //没有登陆跳转方式跳转至登陆网关
				break;
			case 'return':
				return $islogin;//返回方式返回
				break;
			default: //默认方式
				IS_AJAX  ? $this->msg($islogin) : $this->_empty();
				break;
		}
	}

	/**
	 *
	 * 登陆入口
	 */
	public function index(){
		if(IS_POST){ //登陆提交
			if(HOOK('Plugin','Verify','isverify')){ //启动了验证码
				$msgVerify = HOOK('Plugin', 'Verify', 'checkverify', '', '', I('post.verify'));
				!$msgVerify[0] && $this->msg(0,$msgVerify[1]);
			}
			$user = I('post.user');
			$password = I('post.password');
			HOOK('Plugin','Logincooling','coolingcheck', '', '', $user); //登陆冷却钩子

			//密码格式检测
			$formatPassword = is_format_password($password);
			!$formatPassword[0] && $this->msg(0,$formatPassword[1]);

			$mUser = new \Common\Model\SystemUserModel;
			$viewFields = $mUser->viewFields();
			$viewFields['SystemUserRole']=array('role_id','_on'=>'SystemUser.id=SystemUserRole.user_id');
			$viewFields = array_merge($viewFields,$mUser->viewFields(M('SystemRole'),'role_'));
			$viewFields['SystemRole']['_on'] = 'SystemRole.id=SystemUserRole.role_id';
			$mUser->viewFields = $viewFields;
			!($info = $mUser->getInfo($user,'View')) &&  $this->msg(0,L('USER_DOES_NOT_EXIST')); //账号不存在
			
			//密码错误
			if(get_md5_rules($password) !== $info['password']) return $this->_loginError($info, L('PASSWORD_ERROR'));

			if((int)$info['status'] === 0) return $this->_loginError($info, L('USER_IS_LOCK'));

			//登录成功
			unset($info['password']);  //禁止密码外露
			HOOK('Plugin','Singlelogin','addloginInfo', '', '', $info['id']); //单点登录插件记录登陆信息
			session(C('USER_INFO'),$info); //保存session
			M('SystemUser')->execute("update __TABLE__ set `lastloginip`= theloginip,`lastlogintime` = thelogintime, `theloginip`= '".get_client_ip()."',`thelogintime` = '".time()."', `loginnum` = loginnum+1 where (`id` = '".$user."') or (`name` = '".$user."') or (`email` = '".$user."') or (`mobilephone` = '".$user."')");//记录登陆信息
			$this->msg(1, L('LOGIN_SUCCESS'));

		}
		$this->assign('title', L('USER_LOGIN'));
		$this->display();
	}

	/**
	 * 退出接口
	 */
	public function out($return = false){
		session(C('USER_INFO'), null);
		unset($_SESSION[C('SESSION_PREFIX')]);
		cookie(null,C('COOKIE_PREFIX'));
		session_destroy();
		if($return) return true;
		$this->assign('title', L('USER_OUT'));
		$this->msg(1, L('OUT_SUCCESS'));
	}

	/**
	 *
	 * 登陆失败执行的方法
	 * @param $user 用户
	 * @param $msg 提示信息
	 */
	private function _loginError($info = '', $msg){
		HOOK('Plugin','Logincooling', 'coolingaddnum', '', '', $info);//记录登陆错误次数冷却插件钩子
		$this->msg(0,$msg);
	}
}