<?php
// +--------------------------------------------------------------------------------------------
// | Author: Admin <543423@qq.com>
// +--------------------------------------------------------------------------------------------
// | Copyright ThinkAdmin http://www.thinkadmin.cn All rights reserved.
// +--------------------------------------------------------------------------------------------

/**
 * +--------------------------------------------------------------------------------------------
 * |附件对外接口
 * +--------------------------------------------------------------------------------------------
 * |附件接口
 * +--------------------------------------------------------------------------------------------
 */
namespace Api\Controller;
class AttachmentController extends ApiController{

	/**
	 * 输出用户头像
	 */
	public function userhead(){
		$name = I('get.name','a').'.gif';
		$filename = WRITE_PATH.'./Upload/'.'User/Head/'.$_GET['id'].'/'.$name;
		if(!file_exists($filename)) $filename = COMMON_PATH."Data/Img/nophoto.gif";
		$im = getimagesize($filename);
		$handle = fopen($filename, "rb");
		$filesize = filesize($filename);
		$contents = fread($handle, $filesize);
		header("content-type:".$im['mime']);
		echo $contents;
		fclose($handle);
		exit();
	}

	/**
	 * 输出语言JS
	 */
	public function langjs(){
		$mLang = new \Common\Model\SystemLangModel;
		$langType = I('get.lang');
		$module = I('get.module');
		$filename = DATA_PATH.'./System/'.$mLang->getModelName().'./lang/'.$langType.'/'.$module.'_lang.js';
		if(!file_exists($filename)){ //没有存在文件
			$mSystemLang = new \Common\Model\SystemLangModel; //获取模型实例
			$lang = $mSystemLang->getLang($module); //获取语言
			$jsCon = 'var ';
			foreach ($lang as $k=>$v){
				$key = array_keys($lang);
				$thecount = array_keys($key,$k);
				$dou = count($lang)  > ($thecount[0]+1) ? ',' : ';';
				$jsCon .= "$k='$v'$dou";
			}
			\Common\Lib\FileUtil::insertFile($jsCon,DATA_PATH.'./System/'.$mLang->getModelName().'./lang/'.LANG_SET.'/'.$module.'_lang.js',true); //创建文件
		}
		if(!file_exists($filename)) $this->_empty();
		$im = getimagesize($filename);
		$handle = fopen($filename, "rb");
		$filesize = filesize($filename);
		$contents = fread($handle, $filesize);
		header("content-type:text/javascript");
		echo $contents;
		fclose($handle);
		exit();
	}

	/**
	 * 输出附件
	 */
	public function index(){
		if(I('get.type') ==='uploadify'){ //输出uploadify swf
			$filename = COMMON_PATH."Data/Swf/uploadify.swf";
		}
		if(!file_exists($filename)) $this->_empty();
		$im = getimagesize($filename);
		$handle = fopen($filename, "rb");
		$filesize = filesize($filename);
		$contents = fread($handle, $filesize);
		header("content-type:".$im['mime']);
		echo $contents;
		fclose($handle);
	}

}