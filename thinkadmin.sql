-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2014-07-12 11:40:48
-- 服务器版本： 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thinkadmin`
--

-- --------------------------------------------------------

--
-- 表的结构 `tka_plugin_backlistip`
--

CREATE TABLE IF NOT EXISTS `tka_plugin_backlistip` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `ip` char(15) NOT NULL COMMENT 'IP地址',
  `locktime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '锁定截止时间 0为永久锁定',
  `lockmodule` varchar(200) NOT NULL COMMENT '锁定模块',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_UNIQUE` (`ip`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='IP黑名单表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `tka_plugin_backlistip`
--

INSERT INTO `tka_plugin_backlistip` (`id`, `ip`, `locktime`, `lockmodule`, `remark`) VALUES
(1, '127.0.0.2', 0, 'a:2:{i:0;s:5:"Admin";i:1;s:3:"Api";}', '测试');

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_config`
--

CREATE TABLE IF NOT EXISTS `tka_system_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` char(20) NOT NULL COMMENT '配置类型',
  `config` text NOT NULL COMMENT '配置',
  `remark` varchar(200) NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `tka_system_config`
--

INSERT INTO `tka_system_config` (`id`, `type`, `config`, `remark`) VALUES
(1, 'Common', 'a:31:{s:20:"URL_CASE_INSENSITIVE";b:1;s:20:"TMPL_TEMPLATE_SUFFIX";s:4:".dwt";s:16:"TMPL_STRIP_SPACE";b:1;s:13:"INSTALL_ENTER";s:19:"Install/Index/index";s:14:"LANG_SWITCH_ON";b:1;s:16:"LANG_AUTO_DETECT";b:1;s:9:"LANG_LIST";s:5:"zh-cn";s:12:"VAR_LANGUAGE";s:1:"l";s:16:"VISITORS_ROLE_ID";i:1;s:13:"ADMIN_ROLE_ID";i:2;s:16:"TAG_NESTED_LEVEL";i:10;s:8:"VAR_PAGE";s:1:"p";s:9:"SKIN_PATH";s:13:"__ROOT__/Skin";s:16:"DEFAULT_TIMEZONE";s:3:"PRC";s:17:"TMPL_ACTION_ERROR";s:10:"Public:msg";s:19:"TMPL_ACTION_SUCCESS";s:10:"Public:msg";s:12:"SESSION_PATH";s:28:"__NOWEB_PATH__Write/Session/";s:14:"SESSION_EXPIRE";i:3600;s:9:"URL_MODEL";i:0;s:19:"PASSWORD_MIN_LENGTH";i:6;s:19:"PASSWORD_MAX_LENGTH";i:16;s:15:"USER_MIN_LENGTH";i:5;s:15:"USER_MAX_LENGTH";i:16;s:11:"UPDATE_HTTP";s:19:"http://www.tpapp.cn";s:13:"HTML_CACHE_ON";b:1;s:15:"HTML_CACHE_TIME";i:60;s:16:"HTML_FILE_SUFFIX";s:6:".shtml";s:7:"VERSION";s:3:"1.0";s:15:"SHOW_PAGE_TRACE";b:1;s:11:"WAIT_SECOND";i:3;s:6:"UPLOAD";a:17:{s:9:"IS_UPLOAD";b:1;s:4:"SIZE";i:2048576;s:9:"DAN_LIMIT";i:10;s:12:"IS_WATERMARK";b:1;s:14:"WATERMARK_TYPE";i:1;s:14:"WATERMARK_TEXT";s:18:"http://www.xxx.com";s:9:"FONT_SIZE";i:14;s:12:"WATERMARK_XY";i:1;s:16:"WATERMARK_APACHE";i:10;s:8:"IS_THUMB";b:1;s:12:"THUMB_PREFIX";s:6:"thumb_";s:7:"THUMB_W";s:3:"300";s:7:"THUMB_H";s:3:"300";s:10:"THUMB_TYPE";i:2;s:4:"EXTS";a:11:{i:0;s:3:"xls";i:1;s:3:"doc";i:2;s:3:"swf";i:3;s:3:"jpg";i:4;s:3:"gif";i:5;s:4:"jpeg";i:6;s:3:"png";i:7;s:3:"mp3";i:8;s:3:"rar";i:9;s:3:"zip";i:10;s:3:"bmp";}s:5:"TYPES";a:7:{i:0;s:10:"image/jpeg";i:1;s:9:"image/gif";i:2;s:9:"image/png";i:3;s:9:"image/bmp";i:4;s:10:"text/plain";i:5;s:11:"image/pjpeg";i:6;s:29:"application/x-shockwave-flash";}s:8:"SAVEPATH";s:9:"/Uploads/";}}', '全局配置'),
(2, 'Admin', 'a:12:{s:5:"TITLE";s:28:"ThinkAdmin内容管理系统";s:8:"KEYWORDS";s:83:"ThinkAdmin,cms,cmf,开源框架,门户系统,文章系统,开发框架,企业门户";s:11:"DESCRIPTION";s:263:"ThinkAdmin内容管理系统是基于ThinkPHP框架开发的一款内容管理平台,核心内置基础设置，权限控制,扩展机制等，并且通过扩展可以完成插件,工具,模块,模型的任意扩展，功能强大，开发方便，上手容易！";s:13:"DEFAULT_THEME";s:7:"Default";s:12:"MODULE_CLOSE";b:0;s:8:"IS_LOGIN";b:1;s:14:"NO_LOGIN_RULES";a:0:{}s:9:"URL_MODEL";i:0;s:7:"IS_AUTH";b:1;s:13:"NO_AUTH_RULES";a:1:{i:0;s:23:"Admin/Extension/usehook";}s:9:"MAX_LIMIT";i:500;s:5:"LIMIT";i:10;}', 'Admin后台管理模块配置'),
(3, 'Install', 'a:10:{s:5:"TITLE";s:40:"ThinkAdmin内容管理系统安装执行";s:8:"KEYWORDS";s:52:"ThinkAdmin安装,CMS安装,系统安装,程序安装";s:11:"DESCRIPTION";s:263:"ThinkAdmin内容管理系统是基于ThinkPHP框架开发的一款内容管理平台,核心内置基础设置，权限控制,扩展机制等，并且通过扩展可以完成插件,工具,模块,模型的任意扩展，功能强大，开发方便，上手容易！";s:13:"DEFAULT_THEME";s:7:"Default";s:12:"MODULE_CLOSE";b:0;s:8:"IS_LOGIN";b:0;s:14:"NO_LOGIN_RULES";a:0:{}s:9:"URL_MODEL";i:0;s:7:"IS_AUTH";b:0;s:13:"NO_AUTH_RULES";a:0:{}}', 'Install项目安装模块的配置'),
(4, 'Api', 'a:11:{s:5:"TITLE";s:12:"开放接口";s:8:"KEYWORDS";s:116:"开放接口,接口,公共接口,ThinkAdmin,cms,cmf,开源框架,门户系统,文章系统,开发框架,企业门户";s:11:"DESCRIPTION";s:263:"ThinkAdmin内容管理系统是基于ThinkPHP框架开发的一款内容管理平台,核心内置基础设置，权限控制,扩展机制等，并且通过扩展可以完成插件,工具,模块,模型的任意扩展，功能强大，开发方便，上手容易！";s:13:"DEFAULT_THEME";s:7:"Default";s:12:"MODULE_CLOSE";b:0;s:8:"IS_LOGIN";b:0;s:14:"NO_LOGIN_RULES";a:0:{}s:9:"URL_MODEL";i:0;s:7:"IS_AUTH";b:0;s:13:"NO_AUTH_RULES";a:0:{}s:16:"HTML_CACHE_RULES";a:1:{s:11:"login:index";a:1:{i:0;s:33:"{:module}/{:controller}_{:action}";}}}', 'Api开放接口模块配置');

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_lang`
--

CREATE TABLE IF NOT EXISTS `tka_system_lang` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang` char(40) NOT NULL COMMENT '语言名',
  `value` text NOT NULL COMMENT '对应语言值',
  `module` char(20) NOT NULL COMMENT '所属模块',
  `type` char(10) NOT NULL COMMENT '语言类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=666 ;

--
-- 转存表中的数据 `tka_system_lang`
--

INSERT INTO `tka_system_lang` (`id`, `lang`, `value`, `module`, `type`) VALUES
(1, 'VISITOR', '游客', 'Common', 'zh-cn'),
(2, 'VISITORS_ROLE_ID_SET_ERROR', '初始化游客角色id出错', 'Common', 'zh-cn'),
(3, 'ERROR_404', '404错误', 'Common', 'zh-cn'),
(4, 'PAGE_EMPTY_TIP', '啊哦！你访问的页面不存在', 'Common', 'zh-cn'),
(5, 'WEB_HOME', '网站首页', 'Common', 'zh-cn'),
(6, 'SET_HOMEPAGE', '设为主页', 'Common', 'zh-cn'),
(7, 'ADD_TO_FAVORITES', '加入收藏', 'Common', 'zh-cn'),
(8, 'USER_TIP', '账号/邮箱/手机', 'Common', 'zh-cn'),
(9, 'PASSWORD', '密码', 'Common', 'zh-cn'),
(10, 'LOGIN', '登陆', 'Common', 'zh-cn'),
(11, 'VERIFY', '验证码', 'Common', 'zh-cn'),
(12, 'POST_EMPTY', '请求的方法不存在', 'Common', 'zh-cn'),
(13, 'USER_NO_EMPTY_TIP', '账号不能为空', 'Common', 'zh-cn'),
(14, 'PLEASE_ENTER_THE_ACCOUNT', '请输入账号', 'Common', 'zh-cn'),
(15, 'PASSWORD_NO_EMPTY_TIP', '密码不能为空', 'Common', 'zh-cn'),
(16, 'PASSWORD_LEN_ERROR_TIP_A', '密码长度由', 'Common', 'zh-cn'),
(17, 'LEN_ERROR_TIP_B', '个字符组成', 'Common', 'zh-cn'),
(18, 'CLICK_REFRESH', '点击刷新', 'Common', 'zh-cn'),
(19, 'MINUTE', '分钟', 'Common', 'zh-cn'),
(20, 'PASSWORT_FORMA_T_SUCCESS', '密码格式正确', 'Common', 'zh-cn'),
(21, 'USER_DOES_NOT_EXIST', '用户不存在', 'Common', 'zh-cn'),
(22, 'PASSWORD_ERROR', '密码错误', 'Common', 'zh-cn'),
(23, 'USER_IS_LOCK', '账号已锁定', 'Common', 'zh-cn'),
(24, 'LOGIN_SUCCESS', '登陆成功', 'Common', 'zh-cn'),
(27, 'TIP_INFO', '提示信息', 'Common', 'zh-cn'),
(28, 'NO_ACCESS', '没有权限', 'Common', 'zh-cn'),
(29, 'HEAD_TX', '头像', 'Common', 'zh-cn'),
(30, 'DELETE_CACHE', '删除缓存', 'Common', 'zh-cn'),
(31, 'CHANGE_PASSWORD', '修改密码', 'Common', 'zh-cn'),
(32, 'CHANGE_INFO', '修改资料', 'Common', 'zh-cn'),
(33, 'CHANGE_ERROR', '修改失败', 'Common', 'zh-cn'),
(34, 'CHANGE_SUCCESS', '修改成功', 'Common', 'zh-cn'),
(35, 'MY_INFO', '我的资料', 'Common', 'zh-cn'),
(36, 'HELLO', '你好', 'Common', 'zh-cn'),
(37, 'STATUS', '状态', 'Common', 'zh-cn'),
(38, 'ROLE', '角色', 'Common', 'zh-cn'),
(39, 'LOGIN_NUM', '登陆次数', 'Common', 'zh-cn'),
(40, 'OUT', '退出', 'Common', 'zh-cn'),
(41, 'SHORTCUTMENU', '快捷菜单', 'Common', 'zh-cn'),
(42, 'SEARCH_MENU', '搜索菜单', 'Common', 'zh-cn'),
(43, 'SEARCH', '搜索', 'Common', 'zh-cn'),
(44, 'PREV_PAGE', '上一个', 'Common', 'zh-cn'),
(45, 'NEXT_PAGE', '下一个', 'Common', 'zh-cn'),
(46, 'NEXT_ROLL', '向下滚动', 'Common', 'zh-cn'),
(47, 'PREV_ROLL', '向上滚动', 'Common', 'zh-cn'),
(48, 'SX_IFRAME', '刷新框架', 'Common', 'zh-cn'),
(49, 'ALREADY_LOCK', '已锁定', 'Common', 'zh-cn'),
(50, 'NORMAL', '正常', 'Common', 'zh-cn'),
(51, 'TEAM', '团队', 'Common', 'zh-cn'),
(52, 'USED', '常用', 'Common', 'zh-cn'),
(53, 'SUBMIT', '提交', 'Common', 'zh-cn'),
(54, 'SET_SUCCESS', '设置成功', 'Common', 'zh-cn'),
(55, 'SET_ERROR', '设置失败', 'Common', 'zh-cn'),
(56, 'CHANGEPASSWORD', '修改密码', 'Common', 'zh-cn'),
(57, 'CHANGEPASSWORD_TIP', '<ol><li>密码请用数字加字母的组合来提高密码强度</li><li>修改密码前需要验证历史密码</li></ol>', 'Common', 'zh-cn'),
(58, 'OLD_PASSWORD', '旧密码', 'Common', 'zh-cn'),
(59, 'NEW_PASSWORD', '新密码', 'Common', 'zh-cn'),
(60, 'PLESE_INPUT_OLDPASSWORD', '请输入旧密码', 'Common', 'zh-cn'),
(61, 'OLD_PASSWORD_ERROR', '旧密码输入错误', 'Common', 'zh-cn'),
(62, 'OLD_PASSWORD_CORRECT', '旧密码输入正确', 'Common', 'zh-cn'),
(63, 'PASSWORD_STRENGTH', '密码强度', 'Common', 'zh-cn'),
(64, 'WEAK', '弱', 'Common', 'zh-cn'),
(65, 'GENERAL', '一般', 'Common', 'zh-cn'),
(66, 'STRENGTH', '强', 'Common', 'zh-cn'),
(67, 'REPEAT_PASSWORD', '重复密码', 'Common', 'zh-cn'),
(68, 'OLDPASSWORD_FORMAT_ERROR', '旧密码格式错误', 'Common', 'zh-cn'),
(69, 'DATA_VALIDATION_LOAD_TIP', '数据校验中，请稍候...', 'Common', 'zh-cn'),
(70, 'THE_SERVER_IS_BUSY_TIP', '服务器忙活着超时,没有返回数据，请重试', 'Common', 'zh-cn'),
(71, 'INPUT_RIGHT', '输入正确', 'Common', 'zh-cn'),
(72, 'PLESE_INPUT_NEWPASSWORD', '请输入新密码', 'Common', 'zh-cn'),
(73, 'NEWPASSWORD_FORMAT_ERROR', '新密码格式错误', 'Common', 'zh-cn'),
(74, 'PLESE_INPUT_CFPASSWORD', '请重复输入新密码', 'Common', 'zh-cn'),
(75, 'PWDPASSWORD_FORMAT_ERROR', '重复输入密码格式错误', 'Common', 'zh-cn'),
(76, 'REPEAT_PASSWORD_ERROR', '重复密码二次输入不一致', 'Common', 'zh-cn'),
(77, 'PLESE_INPUT_PASSWORD', '请输入密码', 'Common', 'zh-cn'),
(78, 'PASSWORT_FORMA_SUCCESS', '密码格式正确', 'Common', 'zh-cn'),
(79, 'PWDCONFIRM_TIP', '重复密码必须和新密码保持一致', 'Common', 'zh-cn'),
(80, 'SEARCH_MENU_A', '有关', 'Common', 'zh-cn'),
(81, 'SEARCH_MENU_B', '的搜索', 'Common', 'zh-cn'),
(82, 'SEARCH_MENU_C', '没有搜索到相关内容<br>注意:只有在有点击连接的导航菜单才会被搜索', 'Common', 'zh-cn'),
(83, 'THE_DATA_VALIDATION_LOAD_TIP', '有数据正在校验中，请稍候...', 'Common', 'zh-cn'),
(84, 'BASIC_SET', '基本设置', 'Common', 'zh-cn'),
(85, 'SECONDS', '秒', 'Common', 'zh-cn'),
(86, 'WEB_TITLE', '网站标题', 'Common', 'zh-cn'),
(87, 'WEB_KEYWORDS', '网站关键字', 'Common', 'zh-cn'),
(88, 'WEB_DESCRIPTION', '网站描述', 'Common', 'zh-cn'),
(89, 'IS_NUM', '是数字', 'Common', 'zh-cn'),
(90, 'FORMAT_ERROR', '格式错误', 'Common', 'zh-cn'),
(91, 'PLEASE_INPUT_NUM', '请输入一个数字', 'Common', 'zh-cn'),
(92, 'DOU_TIP', '多个请用英文半角","号隔开', 'Common', 'zh-cn'),
(93, 'ZC_HTML_TIP', '支持HTML代码 ', 'Common', 'zh-cn'),
(94, 'PLEASE_INPUT', '请输入', 'Common', 'zh-cn'),
(95, 'NO_EMPTY', '不能为空', 'Common', 'zh-cn'),
(96, 'SYSTEM_NO_INSERT_FILE', '系统已经关闭所有文件写入', 'Common', 'zh-cn'),
(97, 'INSERT_FILE_PATH_NOEMPTY', '请指定写入文件的路径', 'Common', 'zh-cn'),
(98, 'INSERT_FILE_IS_NOFILE', '写入的文件路径不是文件', 'Common', 'zh-cn'),
(99, 'INSERT_NO_FILE', '写入的文件不存在', 'Common', 'zh-cn'),
(100, 'CREATE_FILE_ERROR', '创建文件失败', 'Common', 'zh-cn'),
(101, 'OPERATION_SUCCESS', '操作成功', 'Common', 'zh-cn'),
(102, 'OPERATION_ERROR', '操作失败', 'Common', 'zh-cn'),
(103, 'OPERATING_ERROR', '操作失败', 'Common', 'zh-cn'),
(104, 'ZD_GX', '自动更新', 'Common', 'zh-cn'),
(105, 'OPEN', '打开', 'Common', 'zh-cn'),
(106, 'CLOSE', '关闭', 'Common', 'zh-cn'),
(107, 'YES', '是', 'Common', 'zh-cn'),
(108, 'NO', '否', 'Common', 'zh-cn'),
(109, 'EMAIL_TITLE', '邮件标题', 'Common', 'zh-cn'),
(110, 'EMAIL_SHOU_USER', '收件人', 'Common', 'zh-cn'),
(111, 'EMAIL_TEST_CON_TITLE', '邮件内容', 'Common', 'zh-cn'),
(112, 'MAIL_FORMAT_ERROR', '邮箱格式错误', 'Common', 'zh-cn'),
(113, 'EMAIL_OPEN_ERROR', '系统关闭了邮件发送', 'Common', 'zh-cn'),
(114, 'TEST', '测试', 'Common', 'zh-cn'),
(115, 'EMAIL_TO_ERROR', '邮件发送失败', 'Common', 'zh-cn'),
(116, 'EMAIL_TO_SUCCESS', '邮件发送成功', 'Common', 'zh-cn'),
(117, 'IMG', '图片', 'Common', 'zh-cn'),
(118, 'TEXT', '文字', 'Common', 'zh-cn'),
(119, 'FONT_SIZE', '字体大小', 'Common', 'zh-cn'),
(120, 'RANDOM', '随机', 'Common', 'zh-cn'),
(121, 'TOP', '上', 'Common', 'zh-cn'),
(122, 'BOTTOM', '下', 'Common', 'zh-cn'),
(123, 'LEFT', '左', 'Common', 'zh-cn'),
(124, 'RIGHT', '右', 'Common', 'zh-cn'),
(125, 'CENTER', '中', 'Common', 'zh-cn'),
(126, 'WIDTH', '宽', 'Common', 'zh-cn'),
(127, 'HEIGHT', '高', 'Common', 'zh-cn'),
(128, 'CGXTXZ', '超过系统限制', 'Common', 'zh-cn'),
(129, 'CLEAR', '清理', 'Common', 'zh-cn'),
(130, 'CACHE', '缓存', 'Common', 'zh-cn'),
(131, 'MODULE', '模块', 'Common', 'zh-cn'),
(132, 'WEB_CLEARALL', '全站清理', 'Common', 'zh-cn'),
(133, 'CLEAR_SUCCESS', '清理成功', 'Common', 'zh-cn'),
(134, 'CZCL', '存在部分残留', 'Common', 'zh-cn'),
(135, 'INPUT_EMPTY', '输入为空', 'Common', 'zh-cn'),
(136, 'PLEASE_INPUT_ABC', '请输入字母', 'Common', 'zh-cn'),
(137, 'IDENTIFICATION', '标识', 'Common', 'zh-cn'),
(138, 'OPERATING', '操作', 'Common', 'zh-cn'),
(139, 'Y_ENABLE', '已启用', 'Common', 'zh-cn'),
(140, 'W_ENABLE', '未启用', 'Common', 'zh-cn'),
(141, 'Y_STOPUSE', '已经停用', 'Common', 'zh-cn'),
(142, 'Y_USE', '已启用', 'Common', 'zh-cn'),
(143, 'VIEW', '查看', 'Common', 'zh-cn'),
(144, 'PLUGIN', '插件', 'Common', 'zh-cn'),
(145, 'NAME', '名称', 'Common', 'zh-cn'),
(146, 'VERSION_NUMBER', '升级识别号', 'Common', 'zh-cn'),
(147, 'INSTALL_TIME', '安装时间', 'Common', 'zh-cn'),
(148, 'REMARK', '备注', 'Common', 'zh-cn'),
(149, 'VERSION_UPDATE', '版本更新', 'Common', 'zh-cn'),
(150, 'WFLJFWQ', '无法链接服务器', 'Common', 'zh-cn'),
(151, 'INSTALL', '安装', 'Common', 'zh-cn'),
(152, 'UNINSTALL', '卸载', 'Common', 'zh-cn'),
(153, 'UNINSTALL_TIP', '你确定卸载吗?<br>卸载后将删除对应数据库数据,可能不能恢复,请谨慎操作！', 'Common', 'zh-cn'),
(154, 'INSTALL_TIP', '你确定安装吗?', 'Common', 'zh-cn'),
(155, 'CANCEL', '取消', 'Common', 'zh-cn'),
(156, 'PCLZIP_DB_ERROR', '打包文件出现失败,请重试', 'Common', 'zh-cn'),
(157, 'PL_TIP', '你确定这样操作吗？操作后可能不能恢复！', 'Common', 'zh-cn'),
(158, 'DELETE_SUCCESS', '删除成功', 'Common', 'zh-cn'),
(159, 'DELETE_ERROR', '删除失败', 'Common', 'zh-cn'),
(160, 'NO_UPLOAD_FILE', '没有上传文件', 'Common', 'zh-cn'),
(161, 'UPLOAD', '上传', 'Common', 'zh-cn'),
(162, 'DELETE_PHOTOS', '删除头像', 'Common', 'zh-cn'),
(163, 'UPLOAD_TYPE_ERROR', '不允许的文件类型', 'Common', 'zh-cn'),
(164, 'FILE_CG_SYSTEM_XZ', '文件大小超过系统限制', 'Common', 'zh-cn'),
(165, 'LIMIT', '限制', 'Common', 'zh-cn'),
(166, 'MARK_CF', '标识重复,同名标识已经存在', 'Common', 'zh-cn'),
(167, 'IMPORT_SQLFILE_ERROR', '导入SQL出现失败', 'Common', 'zh-cn'),
(168, 'CONFIG_INSERT_ERROR', '配置文件写入失败', 'Common', 'zh-cn'),
(169, 'CONFIG', '配置', 'Common', 'zh-cn'),
(170, 'PLUGIN_NO_USE', '插件未使用', 'Common', 'zh-cn'),
(171, 'TOOL_NO_USE', '工具未使用', 'Common', 'zh-cn'),
(172, 'CONFIRM', '确定', 'Common', 'zh-cn'),
(173, 'UPDATE_TIP', '正在更新,请稍后', 'Common', 'zh-cn'),
(174, 'Y_GXZ', '已更新', 'Common', 'zh-cn'),
(175, 'UPDATA_FILE_ERROR', '更新文件获取失败', 'Common', 'zh-cn'),
(176, 'PLEASE_INPUT_NAME', '请输入名称', 'Common', 'zh-cn'),
(177, 'NAME_FORMAT_TIP', '名称不能包含特殊字符', 'Common', 'zh-cn'),
(178, 'NO_TSZF', '不能包含特殊字符', 'Common', 'zh-cn'),
(179, 'NAME_LENGTH_ERROR_TIP', '名称由1-16个字符组成', 'Common', 'zh-cn'),
(180, 'NAME_NO_EMPTY_TIP', '名称不能为空', 'Common', 'zh-cn'),
(181, 'PLASE_INPUT_ZZSL', '请输入正整数或0', 'Common', 'zh-cn'),
(182, 'PLEASE_INPUT_ZMSZ', '请输入字母或数字', 'Common', 'zh-cn'),
(183, 'PLEASE_INPUT_ZW', '请输入中文', 'Common', 'zh-cn'),
(184, 'STR_LENGHT', '字符长度', 'Common', 'zh-cn'),
(185, 'FONTSIZE', '字体大小', 'Common', 'zh-cn'),
(186, 'ADD', '添加', 'Common', 'zh-cn'),
(187, 'EDIT', '编辑', 'Common', 'zh-cn'),
(188, 'ADD_IP', '添加IP', 'Common', 'zh-cn'),
(189, 'KEYWORD', '关键字', 'Common', 'zh-cn'),
(190, 'RESET', '重置', 'Common', 'zh-cn'),
(191, 'CHECK_ALL', '全选', 'Common', 'zh-cn'),
(192, 'DELETE', '删除', 'Common', 'zh-cn'),
(193, 'DISPLAY', '显示', 'Common', 'zh-cn'),
(194, 'NUMS', '条', 'Common', 'zh-cn'),
(195, 'TIME', '时间', 'Common', 'zh-cn'),
(196, 'UPDATE', '更新', 'Common', 'zh-cn'),
(197, 'CREATE', '创建', 'Common', 'zh-cn'),
(198, 'ID', '编号', 'Common', 'zh-cn'),
(199, 'SORT_DESC', '▲', 'Common', 'zh-cn'),
(200, 'SORT_ASC', '▼', 'Common', 'zh-cn'),
(201, 'NOINFO', '没有信息', 'Common', 'zh-cn'),
(202, 'FOREVER', '永久', 'Common', 'zh-cn'),
(203, 'CLICK_TWO_EDIT_TIP', '双击编辑', 'Common', 'zh-cn'),
(204, 'IP', 'IP地址', 'Common', 'zh-cn'),
(205, 'HFXXY_LOADING', '合法性校验，请稍候...', 'Common', 'zh-cn'),
(206, 'PLEASE_INPUT_REMARK', '请输入备注', 'Common', 'zh-cn'),
(207, 'LENGTH_TIP', '长度错误', 'Common', 'zh-cn'),
(208, 'REMARK_TIP', '备注长度不超过200个字符', 'Common', 'zh-cn'),
(209, 'IP_FORMAT_ERROR', 'IP格式错误', 'Common', 'zh-cn'),
(210, 'ADD_SUCCESS', '添加成功', 'Common', 'zh-cn'),
(211, 'ADD_ERROR', '添加失败', 'Common', 'zh-cn'),
(212, 'EDIT_SUCCESS', '编辑成功', 'Common', 'zh-cn'),
(213, 'EDIT_ERROR', '编辑失败', 'Common', 'zh-cn'),
(214, 'NO_CK_TIP', '没有选择要操作的项', 'Common', 'zh-cn'),
(215, 'USER_NAME', '用户名', 'Common', 'zh-cn'),
(216, 'PARAMS', '参数', 'Common', 'zh-cn'),
(217, 'CONTROLLER', '控制器', 'Common', 'zh-cn'),
(218, 'ACTION', '操作', 'Common', 'zh-cn'),
(219, 'EXPLAIN', '说明', 'Common', 'zh-cn'),
(220, 'CLICK_SORT', '点击排序', 'Common', 'zh-cn'),
(221, 'DELETEYUE', '清空30天前', 'Common', 'zh-cn'),
(222, 'BL_YIBAI', '保留最近100条', 'Common', 'zh-cn'),
(223, 'DELETE_ALL', '清空所有', 'Common', 'zh-cn'),
(224, 'USER', '用户', 'Common', 'zh-cn'),
(225, 'NO_DATA', '数据不存在', 'Common', 'zh-cn'),
(226, 'CLEAR_ERROR', '清理失败', 'Common', 'zh-cn'),
(227, 'NO_CRUIN', '不用清理', 'Common', 'zh-cn'),
(228, 'DOWNLOAD', '下载', 'Common', 'zh-cn'),
(229, 'TOOL_NAME', '工具名', 'Common', 'zh-cn'),
(230, 'TOOL', '工具', 'Common', 'zh-cn'),
(231, 'LOADING', '请稍后…', 'Common', 'zh-cn'),
(232, 'SORT', '排序', 'Common', 'zh-cn'),
(233, 'RULES', '规则', 'Common', 'zh-cn'),
(234, 'SORT_SUCCESS', '排序成功', 'Common', 'zh-cn'),
(235, 'SORT_ERROR', '排序失败', 'Common', 'zh-cn'),
(236, 'PLEASE_INPUT_RULES', '请输入规则', 'Common', 'zh-cn'),
(237, 'FWQM', '服务器没有返回数据，可能服务器忙，请重试', 'Common', 'zh-cn'),
(238, 'PARENT_RULES', '上级规则', 'Common', 'zh-cn'),
(239, 'TOP_LEVEL', '顶级', 'Common', 'zh-cn'),
(240, 'TREE_A', '└', 'Common', 'zh-cn'),
(241, 'BAN', '禁用', 'Common', 'zh-cn'),
(242, 'CLICK_CHANGE', '点击改变', 'Common', 'zh-cn'),
(243, 'THIS_IN_FIND', '操作项存在子集，不能操作', 'Common', 'zh-cn'),
(244, 'MENU_TYPE', '菜单分类', 'Common', 'zh-cn'),
(245, 'ADD_TYPE', '添加分类', 'Common', 'zh-cn'),
(246, 'SYSTEM', '系统', 'Common', 'zh-cn'),
(247, 'SYSTEM_TIP', '创建后不能修改', 'Common', 'zh-cn'),
(248, 'SYSTEM_REMARK_TIP', '选择系统项创建后,表示该数据是无法删除的', 'Common', 'zh-cn'),
(249, 'NAME_IS_IN', '名称已经存在', 'Common', 'zh-cn'),
(250, 'IS_IN', '已存在', 'Common', 'zh-cn'),
(251, 'NAME_IS_OK', '名称可用', 'Common', 'zh-cn'),
(252, 'SYSTEM_NO_OP', '包含系统项,不能操作', 'Common', 'zh-cn'),
(253, 'TYPE_IN_FIND_DATA', '分类下包含数据,请先清理', 'Common', 'zh-cn'),
(254, 'MENU_LIST', '菜单列表', 'Common', 'zh-cn'),
(255, 'ADD_MENU', '添加菜单', 'Common', 'zh-cn'),
(256, 'FUN', '函数', 'Common', 'zh-cn'),
(257, 'MANAGE', '管理', 'Common', 'zh-cn'),
(258, 'PARENT_MENU', '上级菜单', 'Common', 'zh-cn'),
(259, 'RULES_TYPE', '验证权限规则方式', 'Common', 'zh-cn'),
(260, 'TARGET', '打开方式', 'Common', 'zh-cn'),
(261, 'CHECK_RULES', '选择规则', 'Common', 'zh-cn'),
(262, 'CUSTOM', '自定义', 'Common', 'zh-cn'),
(263, 'NOT', '无', 'Common', 'zh-cn'),
(264, 'MENU', '菜单', 'Common', 'zh-cn'),
(265, 'BACKUP', '备份', 'Common', 'zh-cn'),
(266, 'TABLE', '表', 'Common', 'zh-cn'),
(267, 'IMPORT', '导入', 'Common', 'zh-cn'),
(268, 'SUCCESS', '成功', 'Common', 'zh-cn'),
(269, 'PERFORM', '执行', 'Common', 'zh-cn'),
(270, 'LASE_LOGIN_IP', '上次登录IP', 'Common', 'zh-cn'),
(271, 'LASE_LOGIN_TIME', '上次登录时间', 'Common', 'zh-cn'),
(272, 'THE_LOGIN_IP', '本次登录IP', 'Common', 'zh-cn'),
(273, 'THE_LOGIN_TIME', '本次登录时间', 'Common', 'zh-cn'),
(274, 'NO_RECORD', '无记录', 'Common', 'zh-cn'),
(275, 'RETURN', '返回', 'Common', 'zh-cn'),
(276, 'CATEGORY', '分类', 'Common', 'zh-cn'),
(277, 'FIELD', '字段', 'Common', 'zh-cn'),
(278, 'ADD_FIND', '添加子级', 'Common', 'zh-cn'),
(279, 'LIST', '列表', 'Common', 'zh-cn'),
(280, 'TYPE', '类型', 'Common', 'zh-cn'),
(281, 'MIN_LENGTH', '最小长度', 'Common', 'zh-cn'),
(282, 'MAX_LENGTH', '最大长度', 'Common', 'zh-cn'),
(283, 'REGULAR', '正则', 'Common', 'zh-cn'),
(284, 'REGULAR_ERROR_TIP', '正则效验错误提示', 'Common', 'zh-cn'),
(285, 'ISUNIQUE', '是否唯一', 'Common', 'zh-cn'),
(286, 'YLVIEW', '预览', 'Common', 'zh-cn'),
(287, 'FORM', '表单', 'Common', 'zh-cn'),
(288, 'DEFAULT', '默认', 'Common', 'zh-cn'),
(289, 'CATEGORY_TEMPLATE', '栏目模板', 'Common', 'zh-cn'),
(290, 'LIST_TEMPLATE', '列表模板', 'Common', 'zh-cn'),
(291, 'SHOW_TEMPLATE', '内容模板', 'Common', 'zh-cn'),
(292, 'PLESE_INPUT_XXZM', '请输入小写字母', 'Common', 'zh-cn'),
(293, 'PLEASE_CK', '请选择', 'Common', 'zh-cn'),
(294, 'ORMATTRIBUTE_TIP_TIP', '字段元素附加属性可以设置表单的元素属性，如输入 onclick=&quot;alert(1)&quot;等', 'Common', 'zh-cn'),
(295, 'MAX_LENGTH_IS_MIN_LENG', '最大长度要大于等于最小长度', 'Common', 'zh-cn'),
(296, 'TITLE', '标题', 'Common', 'zh-cn'),
(297, 'COMMONLY_USED_REGULAR', '常用正则', 'Common', 'zh-cn'),
(298, 'MOBILE_PHONE', '手机', 'Common', 'zh-cn'),
(299, 'CHINESE', '中文', 'Common', 'zh-cn'),
(300, 'LETTER', '字母', 'Common', 'zh-cn'),
(301, 'LETTER_NUMBER', '字母数字', 'Common', 'zh-cn'),
(302, 'LETTER_A', '大写字母', 'Common', 'zh-cn'),
(303, 'LETTER_B', '小写字母', 'Common', 'zh-cn'),
(304, 'SHENGFENZHENG_ID', '身份证', 'Common', 'zh-cn'),
(305, 'INTEGER', '整数', 'Common', 'zh-cn'),
(306, 'A_POSITIVE_INTEGER', '正整数', 'Common', 'zh-cn'),
(307, 'NEGTIVE_INTEGER', '负整数', 'Common', 'zh-cn'),
(308, 'DIGITAL', '数字', 'Common', 'zh-cn'),
(309, 'A_POSITIVE_INTEGER_AND_0', '正整数和0', 'Common', 'zh-cn'),
(310, 'NEGATIVE_INTEGER_AND_0', '负整数和0', 'Common', 'zh-cn'),
(311, 'FLOATING_POINT_NUMBER', '浮点数', 'Common', 'zh-cn'),
(312, 'POSITIVE_FLOATING_POINT_NUMBER', '正浮点数', 'Common', 'zh-cn'),
(313, 'NEGATIVE_FLOAT', '负浮点数', 'Common', 'zh-cn'),
(314, 'POSITIVE_FLOATING_POINT_AND_0', '正浮点数和0', 'Common', 'zh-cn'),
(315, 'NEGATIVE_FLOATING_POINT_AND_0', '负浮点数和0', 'Common', 'zh-cn'),
(316, 'MAIL', '邮箱', 'Common', 'zh-cn'),
(317, 'COLOR', '颜色', 'Common', 'zh-cn'),
(318, 'URL', 'URL', 'Common', 'zh-cn'),
(319, 'THE_ACSII_CHARACTER', 'ACSII字符', 'Common', 'zh-cn'),
(320, 'ZIP_CODE', '邮编', 'Common', 'zh-cn'),
(321, 'IP_ADDRESS', 'IP地址', 'Common', 'zh-cn'),
(322, 'NON_EMPTY', '非空', 'Common', 'zh-cn'),
(323, 'THE_PICTURE', '图片', 'Common', 'zh-cn'),
(324, 'THE_COMPRESSED_FILE', '压缩文件', 'Common', 'zh-cn'),
(325, 'DATE', '日期', 'Common', 'zh-cn'),
(326, 'TELEPHONE_NUMBER', '电话号码', 'Common', 'zh-cn'),
(327, 'HYPERLINK', '超链接', 'Common', 'zh-cn'),
(328, 'ERROR', '错误', 'Common', 'zh-cn'),
(329, 'CONCISE', '简洁', 'Common', 'zh-cn'),
(330, 'STANDARD', '标准', 'Common', 'zh-cn'),
(331, 'IS_SAVE_REMOTE', '是否保存远程图片', 'Common', 'zh-cn'),
(332, 'IS_UPLOAD_FILE', '是否支持上传', 'Common', 'zh-cn'),
(333, 'CHARACTER', '字符', 'Common', 'zh-cn'),
(334, 'UPLOAD_FILE_NBZ_FILE', '上传的文件不是标准格式文件', 'Common', 'zh-cn'),
(335, 'UPLOAD_FILE_IS_EMPTY', '不能上传空文件', 'Common', 'zh-cn'),
(336, 'CREATE_FIELD_ERROR', '字段创建失败', 'Common', 'zh-cn'),
(337, 'UPLOAD_ERROR', '上传失败', 'Common', 'zh-cn'),
(338, 'ROLE_NAME', '角色名', 'Common', 'zh-cn'),
(339, 'MEMBER_NUM', '会员数', 'Common', 'zh-cn'),
(340, 'PARENT_LEVEL', '所属上级', 'Common', 'zh-cn'),
(341, 'JC', '基础', 'Common', 'zh-cn'),
(342, 'AJAXLOAD', '数据验证中,请稍后', 'Common', 'zh-cn'),
(343, 'REGIP', '注册IP', 'Common', 'zh-cn'),
(344, 'MOBILEPHONE', '手机', 'Common', 'zh-cn'),
(345, 'REGISTER', '注册', 'Common', 'zh-cn'),
(346, 'LOGIN_TIME', '登录时间', 'Common', 'zh-cn'),
(347, 'LAST', '最后', 'Common', 'zh-cn'),
(348, 'BAT_LOCK', '批量锁定', 'Common', 'zh-cn'),
(349, 'BAT_UNLOCK', '批量解锁', 'Common', 'zh-cn'),
(350, 'BAT_DELETE', '批量删除', 'Common', 'zh-cn'),
(351, 'BAT', '批量', 'Common', 'zh-cn'),
(352, 'MEMBER', '会员', 'Common', 'zh-cn'),
(353, 'LOCK', '锁定', 'Common', 'zh-cn'),
(354, 'PLESE_INPUT_USER_NAME', '请输入用户名', 'Common', 'zh-cn'),
(355, 'USER_LEN_ERROR_TIP_A', '用户名应为', 'Common', 'zh-cn'),
(356, 'USER_FORMAT_ERROR_TIP', '用户名由数字、26个英文字母或者下划线组成', 'Common', 'zh-cn'),
(357, 'USER_NAME_BKY', '用户名不可用', 'Common', 'zh-cn'),
(358, 'RIGHT_LEFT_NO_EMPTY', '两边不能有空格', 'Common', 'zh-cn'),
(359, 'PLESE_INPUT_MAIL', '请输入邮箱', 'Common', 'zh-cn'),
(360, 'EMAIL_FORMAT_ERROR_TIP', '邮箱格式错误', 'Common', 'zh-cn'),
(361, 'EMAIL_IS_IN', '邮箱已经存在', 'Common', 'zh-cn'),
(362, 'PLESE_INPUT_MOBILEPHONE', '请输入手机', 'Common', 'zh-cn'),
(363, 'MOBILEPHONE_FORMAT_ERROR_TIP', '手机格式错误', 'Common', 'zh-cn'),
(364, 'MOBILEPHONE_IS_IN', '手机已存在', 'Common', 'zh-cn'),
(365, 'PLESE_CHECKED_ROLE', '请选择角色', 'Common', 'zh-cn'),
(366, 'USER_ROLE_EMPTY_ERROR', '选择的所属角色不存在', 'Common', 'zh-cn'),
(367, 'QQ_FORMAT_ERROR_TIP', 'QQ号格式错误', 'Common', 'zh-cn'),
(368, 'PHONET_FORMAT_ERROR_TIP', '电话号码格式错误', 'Common', 'zh-cn'),
(369, 'CX_INFO_CF', '出现信息重复', 'Common', 'zh-cn'),
(370, 'BACK_LIST', '返回列表', 'Common', 'zh-cn'),
(371, 'INFO', '信息', 'Common', 'zh-cn'),
(372, 'NOEDIT', '确定后无法修改', 'Common', 'zh-cn'),
(373, 'LAST_LOGIN_IP', '最后登录IP', 'Common', 'zh-cn'),
(374, 'LAST_LOGIN_TIME', '最后登录时间', 'Common', 'zh-cn'),
(375, 'BASIC_INFO', '基本信息', 'Common', 'zh-cn'),
(376, 'NOEDIT_EMPTY_TIP', '不修改留空', 'Common', 'zh-cn'),
(377, 'EXTENDED_INFO', '扩展信息', 'Common', 'zh-cn'),
(378, 'SAVE', '保存', 'Common', 'zh-cn'),
(379, 'SAVE_SUCCESS', '保存成功', 'Common', 'zh-cn'),
(380, 'SAVE_ERROR', '保存失败', 'Common', 'zh-cn'),
(381, 'MINLENG_ERROR', '最小长度错误', 'Common', 'zh-cn'),
(382, 'MAXLENG_ERROR', '最大长度错误', 'Common', 'zh-cn'),
(383, 'ENABLE', '开启', 'Common', 'zh-cn'),
(384, 'DISABILE', '禁止', 'Common', 'zh-cn'),
(385, 'MEMBER_REG', '会员注册', 'Common', 'zh-cn'),
(386, 'COMPANY', '单位', 'Common', 'zh-cn'),
(387, 'BYTE', '字节', 'Common', 'zh-cn'),
(388, 'PARENT', '上级', 'Common', 'zh-cn'),
(389, 'BX_CK', '必须选择', 'Common', 'zh-cn'),
(390, 'CK', '选择', 'Common', 'zh-cn'),
(391, 'REDIRECTION', '重定向', 'Common', 'zh-cn'),
(392, 'CONTENT', '内容', 'Common', 'zh-cn'),
(393, 'DANGQIAN', '当前', 'Common', 'zh-cn'),
(394, 'ADD_CATEGORY_LM', '添加栏目', 'Common', 'zh-cn'),
(395, 'CATEGORY_LM', '栏目', 'Common', 'zh-cn'),
(396, 'UPLOAD_HEAD_SIZA', '头像上传大小', 'Common', 'zh-cn'),
(397, 'HIDDEN', '隐藏', 'Common', 'zh-cn'),
(398, 'CLICK', '点击', 'Common', 'zh-cn'),
(399, 'FF_DY', '非法调用', 'Common', 'zh-cn'),
(400, 'EDITOR_UPLOAD', '编辑器上传', 'Common', 'zh-cn'),
(401, 'STSTEM_NO_UPLOAD', '系统已关闭上传', 'Common', 'zh-cn'),
(402, 'THUMB_DBLSF', '等比例缩放', 'Common', 'zh-cn'),
(403, 'THUMB_FILLED', '缩放后填充', 'Common', 'zh-cn'),
(404, 'THUMB_CENTER', '剧中裁剪', 'Common', 'zh-cn'),
(405, 'THUMB_NORTHWEST', '右上角裁剪', 'Common', 'zh-cn'),
(406, 'THUMB_SOUTHEAST', '右下角裁剪', 'Common', 'zh-cn'),
(407, 'THUMB_FIXED', '固定尺寸缩放', 'Common', 'zh-cn'),
(408, 'ADMIN_LOGIN_TITLE', 'TPAPP内容管理系统', 'Admin', 'zh-cn'),
(409, 'ADMIN_LOGIN', '管理登陆', 'Admin', 'zh-cn'),
(410, 'QT_HOME', '前台首页', 'Admin', 'zh-cn'),
(411, 'ADMIN_HOME', '后台首页', 'Admin', 'zh-cn'),
(412, 'SYSTEM_INFO', '系统信息', 'Admin', 'zh-cn'),
(413, 'MYAPP_VERSION', '程序版本', 'Admin', 'zh-cn'),
(414, 'PHPV', 'PHP版本', 'Admin', 'zh-cn'),
(415, 'OPERATING_SYSTEM', '操作系统', 'Admin', 'zh-cn'),
(416, 'SERVER_SOFTWARE', '服务器软件', 'Admin', 'zh-cn'),
(417, 'UPLOAD_FILES_LIMIT', '上传限制', 'Admin', 'zh-cn'),
(418, 'SHORTCUTMENU_TIP', '<li>当你修改快捷菜单后,需要刷新浏览器才会显示</li><li>如果子级为空，请也不要选择父级菜单。避免出现错误链接菜单</li>', 'Admin', 'zh-cn'),
(419, 'CONFIG_TIP', '<ol><li>所有配置信息都是以文件方式调用的</li><li>公共配置目录:Common/Conf/edit.php</li><li>PHP控制器中获取配置参数 C("键名"),模板中获取配置参数<{$Think.config.键名}></li></ol>', 'Admin', 'zh-cn'),
(420, 'ADDRESS', '地址', 'Admin', 'zh-cn'),
(421, 'CONTACT_MAIL', '联系邮箱', 'Admin', 'zh-cn'),
(422, 'COPYRIGHT', '版权信息', 'Admin', 'zh-cn'),
(423, 'APP_ICP', 'ICP备案信息', 'Admin', 'zh-cn'),
(424, 'COUNT_JS', '第三方统计代码', 'Admin', 'zh-cn'),
(425, 'FOOTER_INFO', '自定义底部信息', 'Admin', 'zh-cn'),
(426, 'CYGL', '词语过滤', 'Admin', 'zh-cn'),
(427, 'CYTH', '过滤替换', 'Admin', 'zh-cn'),
(428, 'SKIN_PATH', '皮肤路径', 'Admin', 'zh-cn'),
(429, 'WAITSECOND', '信息提示页面跳转等待时间', 'Admin', 'zh-cn'),
(430, 'CYTH_TIP', '过滤词替换内容,留空为直接过滤删除', 'Admin', 'zh-cn'),
(431, 'EMAIL_CONFIG_B', '电子邮件设置', 'Admin', 'zh-cn'),
(432, 'EMAIL_FS', '邮件发送', 'Admin', 'zh-cn'),
(433, 'SMTPPSERVER', 'SMTP服务器', 'Admin', 'zh-cn'),
(434, 'SMTPSERVERPORT', 'SMTP端口', 'Admin', 'zh-cn'),
(435, 'SMTPUSERMAIL', '发信人地址', 'Admin', 'zh-cn'),
(436, 'SMTPUSER', '验证用户名', 'Admin', 'zh-cn'),
(437, 'SMTPPASS', '验证密码', 'Admin', 'zh-cn'),
(438, 'MAILTYPE', '邮箱格式类型', 'Admin', 'zh-cn'),
(439, 'MAIL_AUTH', 'SMTP用户身份验证', 'Admin', 'zh-cn'),
(440, 'EMAIL_TIP_A', '开启后可以给用户发送电子邮件。', 'Admin', 'zh-cn'),
(441, 'EMAIL_TIP_B', '一般为25', 'Admin', 'zh-cn'),
(442, 'EMAIL_TIP_C', '如果SMTP服务器要求通过身份验证才可以发邮件，请选择"开启"。', 'Admin', 'zh-cn'),
(443, 'EMAIL_TIP_D', '安全考虑，密码将隐藏', 'Admin', 'zh-cn'),
(444, 'EMAIL_CONFIG', '邮件配置', 'Admin', 'zh-cn'),
(445, 'EMAIL_TO_ERROR', '邮件发送失败', 'Admin', 'zh-cn'),
(446, 'EMAIL_TO_SUCCESS', '邮件发送成功', 'Admin', 'zh-cn'),
(447, 'EMAIL_TEST', '邮件测试', 'Admin', 'zh-cn'),
(448, 'EMAIL_TEST_TIP_FUN', '<ol><li>测试邮件可以测试你的邮件配置是否正确</li><li>测试前确保已经开启了邮件发送功能,如果发送失败请检查配置</li></ol>', 'Admin', 'zh-cn'),
(449, 'EMAIL_TEST_CON', '恭喜您,如果您收到此邮件则代表后台邮件发送设置正确！', 'Admin', 'zh-cn'),
(450, 'EMAIL_CONFIG_TIP', '<ol><li>目前只提供SMTP模式</li><li>配置完成你可以在邮件测试那儿测试是否可以正常发送</li></ol>', 'Admin', 'zh-cn'),
(451, 'UPLOAD_CONFIG_TIP', '<ul><li>上传设置控制着整个网站上传文件</li><li>请根据自己需求配置上传设置</li></ul>', 'Admin', 'zh-cn'),
(452, 'IS_UPLOAD', '是否开启文件上传', 'Admin', 'zh-cn'),
(453, 'UPLOAD_CONFIG', '上传设置', 'Admin', 'zh-cn'),
(454, 'UPLOAD_FILE_SIZE', '单个文件的最大字节', 'Admin', 'zh-cn'),
(455, 'IS_WATERMARK', '是否自动添加水印', 'Admin', 'zh-cn'),
(456, 'ALLOW_FILE_FORMAT', '允许上传的文件格式', 'Admin', 'zh-cn'),
(457, 'WATERMARK_XY', '图片水印位置', 'Admin', 'zh-cn'),
(458, 'WATERMARK_TYPE', '水印方式', 'Admin', 'zh-cn'),
(459, 'WATERMARK_TEXT', '水印文字', 'Admin', 'zh-cn'),
(460, 'WATERMARK_IMG', '水印图片', 'Admin', 'zh-cn'),
(461, 'THUMB_TYPE', '缩略图方式', 'Admin', 'zh-cn'),
(462, 'THUMB_WH', '缩略图宽高', 'Admin', 'zh-cn'),
(463, 'THUMB_WH_TIP', '请输入一个非负数数字,注意：如果只设置一个参数另一个参数设置为0表示另一个参数自动比例缩放', 'Admin', 'zh-cn'),
(464, 'UPLOAD_NUM', '批量上传最大上传个数', 'Admin', 'zh-cn'),
(465, 'UPLOAD_QUEUE_NUM', '批量上传单次最大队列数', 'Admin', 'zh-cn'),
(466, 'UPLOAD_TYOES_YX', '允许上传的文件MIME类型', 'Admin', 'zh-cn'),
(467, 'WATERMARK_IMG_TIP', '请通过FTP修改水印图片', 'Admin', 'zh-cn'),
(468, 'THUMB_PREFIX', '缩略图前缀', 'Admin', 'zh-cn'),
(469, 'THUMB_S_TIP', '缩略图前缀不能为空', 'Admin', 'zh-cn'),
(470, 'WATERMARK_APACHE', '水印透明度', 'Admin', 'zh-cn'),
(471, 'CONFIG_UPLOAD', '修改上传设置', 'Admin', 'zh-cn'),
(472, 'IS_THUMB', '是否自动生成缩略图', 'Admin', 'zh-cn'),
(473, 'UPLOAD_FILE_SIZE_TIP', '单位字节，最大不会超过服务器环境设置', 'Admin', 'zh-cn'),
(474, 'THUMB_W', '缩略图宽', 'Admin', 'zh-cn'),
(475, 'THUMB_H', '缩略图高', 'Admin', 'zh-cn'),
(476, 'CACHE_SET', '缓存设置', 'Admin', 'zh-cn'),
(477, 'CACHE_CONFIG_TIP', '<li>合理设置缓存配置能够大幅度提升网站性能</li><li>目前支持File与Memcache两种类型缓存</li><li>Memcache类型需要服务器的支持</li>', 'Admin', 'zh-cn'),
(478, 'DELETE_CACHE_TIP', '<li>清理缓存时间数据实时更新</li><li>请安需求清理，没有必要全站清理缓存</li><li>如果清理不干净请使用全站清理按钮清理全站缓存</li>', 'Admin', 'zh-cn'),
(479, 'DATA_CACHE_TIME', '数据缓存有效期', 'Admin', 'zh-cn'),
(480, 'DB_FIELDS_CACHE', '是否开启数据表字段缓存', 'Admin', 'zh-cn'),
(481, 'DB_SQL_BUILD_CACHE', '查询的SQL创建缓存', 'Admin', 'zh-cn'),
(482, 'DB_SQL_BUILD_QUEUE', 'SQL缓存队列的缓存方式', 'Admin', 'zh-cn'),
(483, 'DB_SQL_BUILD_LENGTH', 'SQL缓存的队列长度', 'Admin', 'zh-cn'),
(484, 'DATA_CACHE_COMPRESS', '数据缓存是否压缩缓存', 'Admin', 'zh-cn'),
(485, 'HTML_CACHE_ON', '是否开启静态缓存', 'Admin', 'zh-cn'),
(486, 'HTML_CACHE_TIME', '静态缓存有效期', 'Admin', 'zh-cn'),
(487, 'HTML_FILE_SUFFIX', '静态缓存后缀', 'Admin', 'zh-cn'),
(488, 'SELECT_CACHE_TIME', '查询缓存有效期', 'Admin', 'zh-cn'),
(489, 'DATA_CACHE_TYPE', '数据缓存类型', 'Admin', 'zh-cn'),
(490, 'DATA_CACHE_SUBDIR', '是否使用子目录缓存', 'Admin', 'zh-cn'),
(491, 'DATA_PATH_LEVEL', '子目录缓存级别', 'Admin', 'zh-cn'),
(492, 'PERMANENT_CACHE_TIP', '0为永久缓存', 'Admin', 'zh-cn'),
(493, 'MEMCACHE_HOST', '缓存服务器地址', 'Admin', 'zh-cn'),
(494, 'MEMCACHE_PORT', '端口', 'Admin', 'zh-cn'),
(495, 'CACHE_DELETE', '缓存清理', 'Admin', 'zh-cn'),
(496, 'HTML_CACHE', '静态缓存', 'Admin', 'zh-cn'),
(497, 'DATA_CACHE', '数据缓存', 'Admin', 'zh-cn'),
(498, 'OTHER_CACHE', '其他缓存', 'Admin', 'zh-cn'),
(499, 'TOOL_CACHE', '工具缓存', 'Admin', 'zh-cn'),
(500, 'PLUGIN_CACHE', '插件缓存', 'Admin', 'zh-cn'),
(501, 'TEM_CACHE', '项目缓存', 'Admin', 'zh-cn'),
(502, 'MYTEM_CACHE', '临时缓存', 'Admin', 'zh-cn'),
(503, 'TPL_CACHE', '模板缓存', 'Admin', 'zh-cn'),
(504, 'SYSTEM_CACHE', '系统缓存', 'Admin', 'zh-cn'),
(505, 'DATA_CACHE_TIME_TIP', '请输入正整数或0,0为永久缓存', 'Admin', 'zh-cn'),
(506, 'URL_CONFIG_TIP', '<li>URL建议不要随意修改,将影响搜索引擎权重</li><li>如果你修改了需要刷新页面菜单可能才会显示正常，不刷新有些菜单连接可能不正常</li>', 'Admin', 'zh-cn'),
(507, 'URL_MODEL', 'URL模式', 'Admin', 'zh-cn'),
(508, 'PT_TYPE', '普通模式', 'Admin', 'zh-cn'),
(509, 'PATHINFO_TYPE', 'PATHINFO模式', 'Admin', 'zh-cn'),
(510, 'REWRITE_TYPE', 'REWRITE模式', 'Admin', 'zh-cn'),
(511, 'JR_TYPE', '兼容模式', 'Admin', 'zh-cn'),
(512, 'URL_PATHINFO_DEPR', 'PATHINFO模式下的参数分割符', 'Admin', 'zh-cn'),
(513, 'URL_CONFIG', 'URL设置', 'Admin', 'zh-cn'),
(514, 'IS_LUYOU', '是否使用路由', 'Admin', 'zh-cn'),
(515, 'URL_HTML_SUFFIX', 'URL伪静态后缀', 'Admin', 'zh-cn'),
(516, 'URL_SET', 'URL设置', 'Admin', 'zh-cn'),
(517, 'OTHER_SECONFIG_TIP', '<ul><li>安全设置影响网站正常运行请小心设置</li><li>出错可能导致网站不能正常操作和访问</li></ul>', 'Admin', 'zh-cn'),
(518, 'SET_SECONFIG', '安全设置', 'Admin', 'zh-cn'),
(519, 'POST_PATH', '支持POST请求域', 'Admin', 'zh-cn'),
(520, 'POST_PATH_TIP', '请设置允许POST请求的非本域的域名,多个之间用“,”隔开', 'Admin', 'zh-cn'),
(521, 'SESSION_DOMAIN', 'Session作用域', 'Admin', 'zh-cn'),
(522, 'CONFIG_OTHERCONFIG_TIP', '修改安全配置参数', 'Admin', 'zh-cn'),
(523, 'SESSION_DOMAIN_TIP', '请填写你的主域名,非二级域名,不带WWW，如xxx.com', 'Admin', 'zh-cn'),
(524, 'SESSION_YXQ', 'Session有效期', 'Admin', 'zh-cn'),
(525, 'SESSION_PATH', 'Session保存路径', 'Admin', 'zh-cn'),
(526, 'SESSION_PATH_TIP', '请确保路径存在,修改后可能你需要重新登录', 'Admin', 'zh-cn'),
(527, 'ADMIN_BM', '后台别名', 'Admin', 'zh-cn'),
(528, 'ADMIN_NAME_TIP', '后台别名可以隐藏后台,设置后需要从新以新别名访问后台', 'Admin', 'zh-cn'),
(529, 'PLUGS_TIP', '<li>你可以管理你应用的插件</li><li>插件可能会涉及菜单和权限节点，安装插件后请注意设置角色的菜单和权限节点，否则其他角色使用不上插件</li><li>你可以把插件包解压然后通过FTP上传到Noweb/Write/Plugin/插件目录</li>', 'Admin', 'zh-cn'),
(530, 'UPLOAD_PLUGING', '上传插件', 'Admin', 'zh-cn'),
(531, 'REDUCTION_PLUGIN', '还原插件', 'Admin', 'zh-cn'),
(532, 'REDUCTION_TIP', '如果你是其他网站的文件zip包转移到此网站,并且包是从转移网站下载过来的，数据也导入进来，进行的是还原操作请勾选此项，如果你是新安装包，请不要勾选此项，因为勾选此项后不会对配置进行初始化操作,不勾选会配置初始化，初始化后你需要全新安装,如果勾选根据包原始情况处理', 'Admin', 'zh-cn'),
(533, 'USE_PLUGIN', '使用插件', 'Admin', 'zh-cn'),
(534, 'STOP_PLUGIN', '停用插件', 'Admin', 'zh-cn'),
(535, 'MANGAPP_PLUGS', '管理插件', 'Admin', 'zh-cn'),
(536, 'PLUGS_NAME', '插件名称', 'Admin', 'zh-cn'),
(541, 'MENU_DELETE_ERROR', '菜单删除失败', 'Admin', 'zh-cn'),
(542, 'RULES_DELETE_ERROR', '权限规则删除失败', 'Admin', 'zh-cn'),
(543, 'DELETE_TABEL_ERROR', '删除表失败', 'Admin', 'zh-cn'),
(544, 'P_UNINSTALL_DELETE', '请先卸载然后再删除', 'Admin', 'zh-cn'),
(545, 'UPLOAD_FORMAT_BAO_ERROR', '上传包文件不是相应的标准包格式', 'Admin', 'zh-cn'),
(546, 'CSH_B_ERROR', '初始化包配置', 'Admin', 'zh-cn'),
(547, 'YD_ERROR', '移动出错', 'Admin', 'zh-cn'),
(548, 'UPLOAD_SUCCESS', '上传成功', 'Admin', 'zh-cn'),
(549, 'PLUGIN_STATUS_NO_UNINSTALL', '插件状态不是未安装状态,可能已经安装了', 'Admin', 'zh-cn'),
(550, 'TOOL_STATUS_NO_UNINSTALL', '工具状态不是未安装状态,可能已经安装了', 'Admin', 'zh-cn'),
(551, 'MEUN_INSERT_ERROR', '菜单写入失败', 'Admin', 'zh-cn'),
(552, 'THIS_PLUGIN_NO_CONFIG', '此插件没有配置信息', 'Admin', 'zh-cn'),
(553, 'FIELD_CACHE', '字段缓存', 'Admin', 'zh-cn'),
(556, 'TOOL_TIP', '<li>你可以管理你的工具</li><li>工具可能会涉及菜单和权限节点，安装工具后请注意设置角色的菜单和权限节点，否则其他角色使用不上工具</li><li>你可以把工具包解压然后通过FTP上传到Noweb/Write/Tool/插件目录</li>', 'Admin', 'zh-cn'),
(557, 'UPLOAD_TOOL', '上传工具', 'Admin', 'zh-cn'),
(558, 'REDUCTION_TOOL', '还原工具', 'Admin', 'zh-cn'),
(559, 'MANGAPP_TOOL', '管理工具', 'Admin', 'zh-cn'),
(560, 'THIS_TOOL_NO_CONFIG', '此工具没有配置信息', 'Admin', 'zh-cn'),
(561, 'ADD_RULES', '添加规则', 'Admin', 'zh-cn'),
(562, 'RULES_IS_IN', '规则已经存在', 'Admin', 'zh-cn'),
(563, 'RULES_IS_OK', '规则可用', 'Admin', 'zh-cn'),
(564, 'MODEL_CONTENT_LIST_TIP', '<ol><li>内容模型是控制内容数据结构的模型</li><li>你可以根据自身需求添加自定义模型</li><li>自定义模型下面有字段管理,你可以控制每个模型包含的字段</li></ol>', 'Admin', 'zh-cn'),
(565, 'ADD_MODEL', '添加模型', 'Admin', 'zh-cn'),
(566, 'MODEL', '模型', 'Admin', 'zh-cn'),
(567, 'TABLENAME', '表键名', 'Admin', 'zh-cn'),
(568, 'MODEL_TABLENAME_TIP', '由小写字母组成,创建后不能修改', 'Admin', 'zh-cn'),
(569, 'DOMEL_CACHE', '模型缓存', 'Admin', 'zh-cn'),
(570, 'PLEASE_INPUT_MODULE_TABLENAME', '请输入模型表键名', 'Admin', 'zh-cn'),
(571, 'MODULE_TABLENAME_IS_IN', '模型表键名已存在', 'Admin', 'zh-cn'),
(572, 'RULES_NO_EMPTY', '规则不能为空', 'Admin', 'zh-cn'),
(573, 'FIELD_NUM', '字段数', 'Admin', 'zh-cn'),
(574, 'MODEL_INFO', '模型信息', 'Admin', 'zh-cn'),
(575, 'FIELD_NAME', '字段名', 'Admin', 'zh-cn'),
(576, 'AS_NAME', '别名', 'Admin', 'zh-cn'),
(577, 'FORMATTRIBUTE_TIP', '字段元素节点附加属性', 'Admin', 'zh-cn'),
(578, 'MODEL_TEMPLATE_TIP', '只填写模板名，无需后缀,设置好后请确保对应调用模型的站点模块的模板目录下的Content目录下有对应设置的名称模板', 'Admin', 'zh-cn'),
(579, 'DEFAULT_CATEGORY_TEMP_NO_EMPTY', '默认栏目模板不能为空', 'Admin', 'zh-cn'),
(580, 'DEFAULT_LIST_TEMP_NO_EMPTY', '默认列表模板不能为空', 'Admin', 'zh-cn'),
(581, 'DEFAULT_SHOW_TEMP_NO_EMPTY', '默认内容模板不能为空', 'Admin', 'zh-cn'),
(582, 'MODEL_IS_NO', '模型不存在', 'Admin', 'zh-cn'),
(583, 'FIELD_NAME_TIP', '字段名是用来创建表字段的名称，用于获取数据，必须是唯一的，只能由英文字母、数字和下划线组成，并且仅能字母开头，不以下划线结尾', 'Admin', 'zh-cn'),
(584, 'AS_NAME_TIP', '字段别名是添加表单的标题字段，用于提示', 'Admin', 'zh-cn'),
(585, 'FIELD_TIP_TIP', '提示信息将显示在表单下方', 'Admin', 'zh-cn'),
(586, 'FIELD_MIN_LENGTH_TIP', '不限制请留空，如果不能为空请输入1', 'Admin', 'zh-cn'),
(587, 'FIELD_MAX_LENGTH_TIP', '不限制请留空,必须大于等于最小字段', 'Admin', 'zh-cn'),
(588, 'TEXT_LENGTH', '文本框长度', 'Admin', 'zh-cn'),
(589, 'DEFAULT_VALUE', '默认值', 'Admin', 'zh-cn'),
(590, 'IS_PASSWORD', '是否密码框', 'Admin', 'zh-cn'),
(591, 'PLESE_CK_FIELD_TYPE', '请选择字段类型', 'Admin', 'zh-cn'),
(592, 'DANTEXT', '单行文本', 'Admin', 'zh-cn'),
(593, 'DDUOTEXT', '多行文本', 'Admin', 'zh-cn'),
(594, 'EDITOR', '编辑器', 'Admin', 'zh-cn'),
(595, 'REDIO', '选项', 'Admin', 'zh-cn'),
(596, 'NUMBER', '数字', 'Admin', 'zh-cn'),
(597, 'DATE_TIME', '日期时间', 'Admin', 'zh-cn'),
(598, 'WNZD', '万能字段', 'Admin', 'zh-cn'),
(599, 'CREATE_TABLE_ERROR', '建表失败', 'Admin', 'zh-cn'),
(600, 'YOU_SG_DELETE_CREATE_TABLE', '你需要手工删除模型创建的表', 'Admin', 'zh-cn'),
(601, 'FIELD_TYPE_BCZ', '字段类型不存在', 'Admin', 'zh-cn'),
(602, 'CREATE_FIELD_ERROR', '创建字段失败', 'Admin', 'zh-cn'),
(603, 'SG_DELETE_FIELD', '请手工删除创建的字段', 'Admin', 'zh-cn'),
(604, 'TEXT_FIELD_WIDTH', '文本域宽', 'Admin', 'zh-cn'),
(605, 'TEXT_FIELD_HEIGHT', '文本域高', 'Admin', 'zh-cn'),
(606, 'IS_YX_HTML', '是否允许HTML', 'Admin', 'zh-cn'),
(607, 'SEARCH_CONDITION', '作为搜索条件', 'Admin', 'zh-cn'),
(608, 'AS_THE_STATION_SEARCH', '作为全站搜索', 'Admin', 'zh-cn'),
(609, 'IS_COLOR', '是否使用颜色样式', 'Admin', 'zh-cn'),
(610, 'IS_WEIGHT', '是否使用加粗样式', 'Admin', 'zh-cn'),
(611, 'EDITOR_TYPE', '编辑器样式', 'Admin', 'zh-cn'),
(612, 'MODEL_FIELD_FORM_TIP', '例如：&lt;input type=&quot;text&quot; name=&quot;myname&quot; id=&quot;voteid&quot; value=&quot;{__FIELD_VALUE__}&quot; style=&quot;50&quot; /&gt;', 'Admin', 'zh-cn'),
(613, 'UPLOAD_MODEL', '上传模型', 'Admin', 'zh-cn'),
(614, 'MODEL_FILE', '模型文件', 'Admin', 'zh-cn'),
(615, 'CK_MODEL_FILE', '选择模型文件', 'Admin', 'zh-cn'),
(616, 'MODEL_MEMBER_LIST_TIP', '<ol><li>会员模型可以扩展出不同会员字段信息</li><li>内容模型继承了继承字段信息，如用户名，密码，邮箱等基础信息</li><li>删除会员模型时，会同样清理掉对应模型会员，请小心删除</li><li>如果你不确定，需要删除模型，请使用转移功能转移模型角色</li></ol>', 'Admin', 'zh-cn'),
(617, 'USER_FIELD_MANG', '<li>会员模型会继承会员继承模型，所以部分基础字段名是不可用的</li><li>id字段、uid字段、style字段已被系统自动默认生成，因为这些字段必须，所以无需创建</li>', 'Admin', 'zh-cn'),
(618, 'ROLELIST_TIP', '<ol><li>角色是用户的集合方便用户统一管理设置区分</li><li>你可以控制某些角色拥有那些权限规则</li><li>要删除角色时请先清空他的子角色和转移或清空该角色下面的用户</li></ol>', 'Admin', 'zh-cn'),
(619, 'ADD_ROLE', '添加角色', 'Admin', 'zh-cn'),
(620, 'THIS_IN_REG', '该角色是用户注册默认所属角色<br/>请先在会员配置修改注册默认所属角色组', 'Admin', 'zh-cn'),
(621, 'DELETE_ROLE_INUSER', '删除的角色用存在用户,请先转移角色下的用户', 'Admin', 'zh-cn'),
(622, 'SET_RULES', '设置权限', 'Admin', 'zh-cn'),
(623, 'SET_NO_ROLE', '设置的角色不存在', 'Admin', 'zh-cn'),
(624, 'MEMBER_LIST_TIP', '<ul><li>你可以管理你的会员,查看会员信息</li><li>你可以新增你的会员，编辑会员，删除会员等</li></ul>', 'Admin', 'zh-cn'),
(625, 'ADD_USER', '添加用户', 'Admin', 'zh-cn'),
(626, 'MEMBER_SET', '会员设置', 'Admin', 'zh-cn'),
(627, 'IS_REG_USER', '是否开放注册', 'Admin', 'zh-cn'),
(628, 'DEFAULT_REG_ROLE_ID', '注册默认所属角色组', 'Admin', 'zh-cn'),
(629, 'IS_LOGIN_NUM_LOCK', '是否开启登录错误次数检测', 'Admin', 'zh-cn'),
(630, 'LOGIN_ERROR_MINUTE', '登录错误次数过多冷却时间', 'Admin', 'zh-cn'),
(631, 'USER_MIN_LENGTH', '用户名最小长度', 'Admin', 'zh-cn'),
(632, 'BAN_USER_NAME', '用户名禁止包含字符', 'Admin', 'zh-cn'),
(633, 'PASSWORD_MAX_LENGTH', '密码最大长度', 'Admin', 'zh-cn'),
(634, 'PASSWORD_MIN_LENGTH', '密码最小长度', 'Admin', 'zh-cn'),
(635, 'USER_MAX_LENGTH', '用户名最大长度', 'Admin', 'zh-cn'),
(636, 'GETPASSWORD_TIME', '密码找回链接有效期', 'Admin', 'zh-cn'),
(637, 'CONFIG_MEMBER_TIP', '<ul><li>你可以合理配置会员相关配置</li><li>你可以开启注册和关闭注册</li><li>你可以限制用户名不能包含违禁字符</li></ul>', 'Admin', 'zh-cn'),
(638, 'CATEGORY_TIP', '<ul><li>栏目是内容的一个集合</li><li>每个站点模块都可以调用栏目内容</li><li>栏目本身不具有访问性，只内容的集合分类，但是不同站点模块可以调用栏目及栏目内容，并根据栏目设置进行显示输出</li></ul>', 'Admin', 'zh-cn'),
(639, 'RECOMMEND_TIP', '<li>推荐位是方便内容推荐到不同页面的标签</li><li>你可以新增和管理你的推荐位,在内容发布时就可以使用它们了</li>', 'Admin', 'zh-cn'),
(640, 'ADD_RECOMMEND', '添加推荐位', 'Admin', 'zh-cn'),
(641, 'RECOMMEND', '推荐位', 'Admin', 'zh-cn'),
(642, 'EDIT_RECOMMEND', '编辑推荐位', 'Admin', 'zh-cn'),
(643, 'DELETE_RECOMMEND', '删除推荐位', 'Admin', 'zh-cn'),
(644, 'CATEGORY_LM_REDIRECTION_TIP', '栏目重定向，输入一个URL地址访问此栏目自动转向到填写的URL地址', 'Admin', 'zh-cn'),
(645, 'CONTENT_MODEL_TIP', '<li>默认系统已经生成字段id、style、createtime,updatetime,createip,status,createuser,click,recommend,catid等字段,不用手动创建这些名称字段.</li><li>后台管理列表默认调用的主名称为title字段，如果你有一个名称请创建一个title字段存放，否则后台管理内容列表内容字段没有数据</li>', 'Admin', 'zh-cn'),
(646, 'CONTENT_LIST_TIP', '<li>你可以管理当前栏目的内容</li><li>内容删除后无法恢复，如果不使用请改变状态为隐藏，这样就不会显示了</li>', 'Admin', 'zh-cn'),
(647, 'NOTFOUND_TIP_B', '请检查您输入的网址是否正确，或者点击链接继续浏览空间', 'Common', 'zh-cn'),
(648, 'YOU_CAN_CLICK_THE', '你可以点击', 'Common', 'zh-cn'),
(649, 'RETURN_HOME_PAGE', '返回首页', 'Common', 'zh-cn'),
(650, 'MODULE_CLOSE_TIP', '你访问的站点正在维护，请稍后访问…', 'Common', 'zh-cn'),
(651, 'FORGET_PASSWORD', '忘记密码', 'Common', 'zh-cn'),
(652, 'NO_MEMBER_TIP', '还不是本站会员', 'Common', 'zh-cn'),
(653, 'IMMEDIATELY', '立即', 'Common', 'zh-cn'),
(654, 'COOPERATION_USER_TIP', '您可以用合作伙伴账号登录', 'Common', 'zh-cn'),
(655, 'USER_LOGIN', '会员登录', 'Common', 'zh-cn'),
(656, 'VERIFY_NO_EMPTY', '验证码不能为空', 'Common', 'zh-cn'),
(657, 'PAGE_AUTOMATICALLY', '页面自动', 'Common', 'zh-cn'),
(658, 'JUMP', '跳转', 'Common', 'zh-cn'),
(659, 'WAITING_TIME', '等待时间', 'Common', 'zh-cn'),
(660, 'OUT_SUCCESS', '注销成功', 'Common', 'zh-cn'),
(661, 'NO_INSTALL_TIP', '没有安装，请先安装再操作', 'Common', 'zh-cn'),
(662, 'STATUS_NO_UNINSTALL', '不是未安装状态,可能已经安装了', 'Common', 'zh-cn'),
(663, 'STATUS_INUNINSTALL', '已经是卸载状态', 'Common', 'zh-cn'),
(664, 'PLEASE_STOP_USE', '请先停止使用', 'Common', 'zh-cn'),
(665, 'USER_OUT', '用户注销', 'Common', 'zh-cn');

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_menu`
--

CREATE TABLE IF NOT EXISTS `tka_system_menu` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `name` char(16) NOT NULL COMMENT '菜单名称',
  `url` text COMMENT '菜单URL',
  `color` char(7) DEFAULT NULL COMMENT '菜单颜色',
  `bold` tinyint(1) DEFAULT NULL COMMENT '是否加粗',
  `pid` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '所属父级',
  `sort` int(6) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `fun` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否使用内置URL函数',
  `target` char(16) DEFAULT NULL COMMENT '打开方式',
  `remark` varchar(200) DEFAULT NULL COMMENT '菜单备注',
  `rules` varchar(200) NOT NULL COMMENT '菜单规则',
  `rulestype` enum('and','or') NOT NULL DEFAULT 'and' COMMENT '验证码规则方式，支持and和or',
  `menutype_id` tinyint(3) unsigned NOT NULL COMMENT '菜单类型id',
  PRIMARY KEY (`id`),
  KEY `fk_tpapp_menu_tpapp_menutype1_idx` (`menutype_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='菜单表' AUTO_INCREMENT=29 ;

--
-- 转存表中的数据 `tka_system_menu`
--

INSERT INTO `tka_system_menu` (`id`, `name`, `url`, `color`, `bold`, `pid`, `sort`, `status`, `fun`, `target`, `remark`, `rules`, `rulestype`, `menutype_id`) VALUES
(1, '面板', '', '', 0, 0, 1, 1, 0, '', '面板菜单，包含用户本人的相关操作', 'Admin-Panel', 'and', 1),
(2, '全局', '', '', 0, 0, 2, 1, 0, '', '全局菜单，包含系统权限相关设置操作', 'Admin-Global', 'and', 1),
(3, '会员', '', '', 0, 0, 3, 1, 0, '', '会员菜单，包含会员管理等操作', 'Admin-Member', 'and', 1),
(4, '内容', '', '', 0, 0, 4, 1, 0, '', '内容菜单，管理网站内容', 'Admin-Content', 'and', 1),
(5, '工具', '', '', 0, 0, 5, 1, 0, '', '工具菜单，主要是管理工具所产生的相关操作', 'Admin-Tool', 'and', 1),
(6, '插件', '', '', 0, 0, 6, 1, 0, '', '插件菜单，主要是管理插件所产生的相关操作', 'Admin-Plugin', 'and', 1),
(7, '扩展', '', '', 0, 0, 7, 1, 0, '', '扩展菜单，主要是管理工具,插件,模块等扩展功能,可以对站点扩展各种功能', 'Admin-Extension', 'and', 1),
(8, '修改密码', 'Admin/User/changepassword', '', 0, 1, 1, 1, 1, '', '用户修改密码菜单', 'Admin/User/changepassword', 'and', 1),
(9, '工具管理', 'Admin/Extension/tool', '', 0, 7, 1, 1, 1, '', '工具扩展可以管理工具，查看工具和对工具的扩展', 'Admin/Extension/tool', 'and', 1),
(10, '插件管理', 'Admin/Extension/plugin', '', 0, 7, 2, 1, 1, '', '插件扩展可以扩展系统插件，管理系统已安装插件', 'Admin/Extension/plugin', 'and', 1),
(11, '内容模型', 'Admin/Model/content', '', 0, 7, 3, 1, 1, '', '内容模型扩展可以对内模型进行扩展', 'Admin/Model/content', 'and', 1),
(12, '会员模型', 'Admin/Model/member', '', 0, 7, 4, 1, 1, '', '会员模型扩展可以根据不同需求建立不用类型的用户字段信息满足要求', 'Admin/Model/member', 'and', 1),
(27, 'IP黑名单', NULL, NULL, NULL, 6, 1, 1, 0, NULL, NULL, 'Admin-Plugin-Backlistip', 'and', 1),
(28, '管理IP黑名单', 'Admin/Extension/usehook?type=plugin&name=Backlistip&hookc=Run&hooka=index', NULL, NULL, 27, 1, 1, 1, NULL, NULL, 'Admin/Extension/usehook/Plugin/Backlistip/Run/index', 'and', 1);

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_menutype`
--

CREATE TABLE IF NOT EXISTS `tka_system_menutype` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单类型id',
  `name` char(16) NOT NULL COMMENT '菜单类型名称',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否属于系统,属于系统不能删除',
  `remark` varchar(200) DEFAULT NULL COMMENT '菜单类型备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='菜单类型表' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `tka_system_menutype`
--

INSERT INTO `tka_system_menutype` (`id`, `name`, `system`, `remark`) VALUES
(1, '后台菜单', 1, '后台管理拥有的所有菜单'),
(2, '企业站主导航', 1, '主导航菜单'),
(3, '企业站底部菜单', 1, '底部菜单');

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_role`
--

CREATE TABLE IF NOT EXISTS `tka_system_role` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `name` char(16) NOT NULL COMMENT '角色名',
  `pid` int(16) unsigned NOT NULL DEFAULT '0' COMMENT '角色所属父级',
  `system` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是系统角色。系统角色的用户不允许删除,1是0否',
  `sort` int(6) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `remark` varchar(200) DEFAULT NULL COMMENT '角色备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='角色表' AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `tka_system_role`
--

INSERT INTO `tka_system_role` (`id`, `name`, `pid`, `system`, `sort`, `remark`) VALUES
(1, '游客', 0, 1, 1, '未登录用户所属的角色组'),
(2, '超级管理员', 0, 1, 2, '超级管理员账号所属角色'),
(3, '普通会员', 0, 0, 3, '普通会员,默认注册用户所属角色'),
(4, '普通管理', 0, 0, 1, '普通管理');

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_role_rules`
--

CREATE TABLE IF NOT EXISTS `tka_system_role_rules` (
  `role_id` int(6) unsigned NOT NULL COMMENT '角色id',
  `rules_id` int(6) unsigned NOT NULL COMMENT '规则id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色规则表';

--
-- 转存表中的数据 `tka_system_role_rules`
--

INSERT INTO `tka_system_role_rules` (`role_id`, `rules_id`) VALUES
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(4, 10),
(4, 11),
(4, 12),
(4, 13),
(4, 14),
(4, 15),
(4, 17),
(4, 18),
(4, 16),
(4, 19),
(4, 20),
(4, 21),
(4, 46),
(4, 53),
(4, 54),
(4, 55),
(4, 56),
(4, 57),
(4, 58);

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_rules`
--

CREATE TABLE IF NOT EXISTS `tka_system_rules` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id',
  `title` char(16) NOT NULL COMMENT '规则名',
  `rules` varchar(200) NOT NULL COMMENT '规则',
  `pid` int(6) unsigned NOT NULL COMMENT '所属父级,默认为0表示顶级',
  `sort` int(6) unsigned NOT NULL DEFAULT '1' COMMENT '排序,默认1',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态默认1使用，0禁止',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rules` (`rules`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='权限规则表' AUTO_INCREMENT=59 ;

--
-- 转存表中的数据 `tka_system_rules`
--

INSERT INTO `tka_system_rules` (`id`, `title`, `rules`, `pid`, `sort`, `status`, `remark`) VALUES
(1, '后台管理', 'Admin/Index/index', 0, 1, 1, '后台管理权限规则，用户拥有此规则才能进入后台'),
(2, '快捷菜单', 'Admin/Index/shortcutmenu', 1, 2, 1, '后台用户设置快捷菜单'),
(3, '搜索菜单', 'Admin/Index/searchmenu', 1, 3, 1, '后台用户搜索菜单功能'),
(4, '面板', 'Admin-Panel', 1, 1, 1, '后台面板菜单权限规则'),
(5, '全局', 'Admin-Global', 1, 2, 1, '后台全局菜单权限规则'),
(6, '会员', 'Admin-Member', 1, 3, 1, '后台菜单菜单权限规则'),
(7, '内容', 'Admin-Content', 1, 4, 1, '后台内容菜单权限规则'),
(8, '工具', 'Admin-Tool', 1, 5, 1, '后台工具菜单权限规则'),
(9, '插件', 'Admin-Plugin', 1, 6, 1, '后台插件菜单权限规则'),
(10, '扩展', 'Admin-Extension', 1, 7, 1, '后台扩展菜单权限规则'),
(11, '修改密码', 'Admin/User/changepassword', 4, 1, 1, '后台用户修改密码权限规则'),
(12, '工具管理', 'Admin/Extension/tool', 10, 2, 1, '工具列表，显示工具列表'),
(13, '插件管理', 'Admin/Extension/plugin', 10, 1, 1, '插件列表，显示插件列表'),
(14, '内容模型', 'Admin/Model/content', 10, 3, 1, '内容模型管理'),
(15, '会员模型', 'Admin/Model/member', 10, 4, 1, '会员模型扩展'),
(16, '上传插件', 'Admin/Extension/upplugin', 13, 1, 1, '上传插件'),
(17, '停用插件', 'Admin/Extension/pluginstop', 13, 6, 1, '停用插件'),
(18, '启用插件', 'Admin/Extension/pluginuse', 13, 5, 1, '启用插件'),
(19, '安装插件', 'Admin/Extension/inplugin', 13, 3, 1, '安装插件'),
(20, '卸载插件', 'Admin/Extension/unplugin', 13, 4, 1, '卸载插件'),
(21, '下载插件', 'Admin/Extension/downplugin', 13, 2, 1, '下载插件'),
(40, '登录冷却插件', 'Admin-Plugin-Logincooling', 9, 2, 1, '登录冷却插件权限规则'),
(41, '配置', 'Admin/Extension/usehook/Plugin/Logincooling/Run/config', 40, 1, 1, '配置登录冷却插件权限规则'),
(42, '单点登录插件', 'Admin-Plugin-Singlelogin', 9, 3, 0, '单点登录插件权限规则'),
(43, '配置', 'Admin/Extension/usehook/Plugin/Singlelogin/Run/config', 42, 1, 0, '配置单点登录插件权限规则'),
(44, '验证码插件', 'Admin-Plugin-Verify', 9, 4, 1, '验证码插件权限规则'),
(45, '配置', 'Admin/Extension/usehook/Plugin/Verify/Run/config', 44, 1, 1, '配置验证码插件权限规则'),
(46, '删除插件', 'Admin/Extension/delplugin', 13, 5, 1, '删除插件'),
(53, 'IP黑名单插件', 'Admin-Plugin-Backlistip', 9, 1, 1, 'IP黑名单插件菜单权限规则'),
(54, '配置IP黑名单', 'Admin/Extension/usehook/Plugin/Backlistip/Run/config', 53, 1, 1, '配置IP黑名单插件权限规则'),
(55, '管理IP黑名单', 'Admin/Extension/usehook/Plugin/Backlistip/Run/index', 53, 1, 1, '管理IP黑名单插件权限规则'),
(56, '添加IP黑名单', 'Admin/Extension/usehook/Plugin/Backlistip/Run/add', 55, 1, 1, '添加IP黑名单插件权限规则'),
(57, '编辑IP黑名单', 'Admin/Extension/usehook/Plugin/Backlistip/Run/edit', 55, 1, 1, '编辑IP黑名单插件权限规则'),
(58, '删除IP黑名单', 'Admin/Extension/usehook/Plugin/Backlistip/Run/delete', 55, 1, 1, '删除IP黑名单插件权限规则');

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_user`
--

CREATE TABLE IF NOT EXISTS `tka_system_user` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `name` char(16) NOT NULL COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `email` char(32) DEFAULT NULL COMMENT '邮箱',
  `mobilephone` char(11) DEFAULT NULL COMMENT '手机',
  `status` tinyint(1) unsigned NOT NULL COMMENT '状态,0锁定 1正常',
  `createtime` int(10) unsigned NOT NULL COMMENT '创建（注册）时间',
  `regip` char(15) NOT NULL COMMENT '注册ip',
  `loginnum` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登陆次数',
  `lastloginip` char(15) NOT NULL COMMENT '上次登录ip',
  `lastlogintime` int(10) unsigned NOT NULL COMMENT '上次登录时间',
  `theloginip` char(15) NOT NULL COMMENT '本次登录ip',
  `thelogintime` int(10) unsigned NOT NULL COMMENT '本次登录时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '用户备注',
  `modelid` int(11) unsigned NOT NULL COMMENT '模型id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `mobilephone` (`mobilephone`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户基础表' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `tka_system_user`
--

INSERT INTO `tka_system_user` (`id`, `name`, `password`, `email`, `mobilephone`, `status`, `createtime`, `regip`, `loginnum`, `lastloginip`, `lastlogintime`, `theloginip`, `thelogintime`, `remark`, `modelid`) VALUES
(1, 'admin', '78da2e184bbfd04bf9ca0ee710a40d42', '543423@qq.com', '15184490695', 1, 1389854339, '127.0.0.1', 119, '127.0.0.1', 1405072650, '127.0.0.1', 1405127776, '', 0),
(2, 'sasasasa', '78da2e184bbfd04bf9ca0ee710a40d42', '14385195@qq.com', '15184490693', 1, 1401160381, '127.0.0.1', 110, '127.0.0.1', 1405136031, '127.0.0.1', 1405154641, '', 2),
(3, 'aaaaaa', '3c74cca00ac2931381fce9533bad7090', NULL, NULL, 1, 1401247327, '127.0.0.1', 0, '', 0, '', 0, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_user_role`
--

CREATE TABLE IF NOT EXISTS `tka_system_user_role` (
  `user_id` mediumint(8) unsigned NOT NULL COMMENT '用户id',
  `role_id` int(6) unsigned NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_tpapp_user_has_tpapp_role_tpapp_role1_idx` (`role_id`),
  KEY `fk_tpapp_user_has_tpapp_role_tpapp_user_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与角色关系表';

--
-- 转存表中的数据 `tka_system_user_role`
--

INSERT INTO `tka_system_user_role` (`user_id`, `role_id`) VALUES
(1, 2),
(2, 4),
(3, 3);

-- --------------------------------------------------------

--
-- 表的结构 `tka_system_user_shortcutmenu`
--

CREATE TABLE IF NOT EXISTS `tka_system_user_shortcutmenu` (
  `user_id` mediumint(8) unsigned NOT NULL COMMENT '用户id',
  `menu_id` int(6) unsigned NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`user_id`,`menu_id`),
  KEY `fk_tpapp_user_has_tpapp_menu_tpapp_menu1_idx` (`menu_id`),
  KEY `fk_tpapp_user_has_tpapp_menu_tpapp_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户快捷菜单表';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
