// JavaScript Document
var confirmbtn = [
		{
			name:CONFIRM,
			callback:function () {  
			       var iframe = this.iframe.contentWindow;
				   if (iframe.document.body) {
        	                var submits=iframe.document.getElementById('dosubmit');
						    submits.click();
							return false;
                   }else{
						return false;  
				   }
			},
			focus:true
		},
		{name:CLOSE}
];
$(function(){
	//动态载入css
    var c = readCookie('style');
    if (c && (c =='style1' ||c =='style2' ||c =='style3')) {
        switchStylestyle(c);
    }else{
		switchStylestyle('style1');
	}
    //皮肤切换
    $('.skin').on('click focus', 'li',
    function(e) {
        switchStylestyle($(this).attr('id'), true);
    });
	if($("#addform").length){
	$.formValidator.initConfig({formID:'addform',debug:true,
			onSuccess:function(){
			     $("#addform").ajaxSubmit({
				     success:function(msg){
					      window.top.msg(msg.status,msg.message);
						  if(msg.status) {
							  $("input[type=text],input[type=password],textarea").val('');
						  };
					      btn_is_disabled(true);
				     },
			         timeout: 20000,
				     statusCode: {404: function() {
					          window.top.msg(0,POST_EMPTY);
					          btn_is_disabled(true);
                          }
			         },
				     error: function (XMLHttpRequest, textStatus, errorThrown) {
                          window.top.msg(0,textStatus);
					      btn_is_disabled(true);
                     }
			    });
		        btn_is_disabled(false);
				return false;
			},
			onError:function(msg,obj,errorlist){
			    btn_is_disabled(true);
			},
			submitAfterAjaxPrompt:AJAXLOAD
	 });
	}
	
	if($("#editform").length){
	$.formValidator.initConfig({formID:'editform',debug:true,
			onSuccess:function(){
			     $("#editform").ajaxSubmit({
				     success:function(msg){
					      window.top.msg(msg.status,msg.message);
					      btn_is_disabled(true);
				     },
			         timeout: 20000,
				     statusCode: {404: function() {
					          window.top.msg(0,POST_EMPTY);
					          btn_is_disabled(true);
                          }
			         },
				     error: function (XMLHttpRequest, textStatus, errorThrown) {
                          window.top.msg(0,textStatus);
					      btn_is_disabled(true);
                     }
			    });
		        btn_is_disabled(false);
				return false;
			},
			onError:function(msg,obj,errorlist){
			    btn_is_disabled(true);
			},
			submitAfterAjaxPrompt:AJAXLOAD
	 });
	}
	if($("#dialogform").length){
	$.formValidator.initConfig({formID:"dialogform",debug:true,
			onSuccess:function(){
			     $("#dialogform").ajaxSubmit({
				     success:function(msg){
					      if(msg.status){ 
						      window.top.refreshfind(); 
							  window.top.msg(msg.status,msg.message);
						  }else{
							  window.top.msg(msg.status,msg.message,true);
						  }
				     },
			         timeout: 20000,
				     statusCode: {404: function() {
					          window.top.msg(0,POST_EMPTY,true);

                          }
			         },
				     error: function (XMLHttpRequest, textStatus, errorThrown) {
                          window.top.msg(0,textStatus,true);
                     }
			    })
			},
			onError:function(msg,obj,errorlist){
				
			},
			submitAfterAjaxPrompt :THE_DATA_VALIDATION_LOAD_TIP
	 });
	}
	//全选
	$(".box_all").click(function(){
		if(this.checked){
		    $("input[type='checkbox'][disabled!='disabled']").each(function(){
				this.checked = true;						
			})
	    }else{
			$("input[type='checkbox'][disabled!='disabled']").each(function(){
				this.checked = false;						
			})
		}				 
	})
})
function ckall(obj,str1,str2,str3,len){
	if(obj.checked){
		if(len==1){
			if(str1){
		      $("."+str1).each(function(i,e){
			      e.checked = true;
		      })  
	        }
			if(str3){
		      $("."+str3).each(function(i,e){
			      e.checked = true;
		      })  
	        }
			if(str2){
		      $("."+str2).each(function(i,e){
			      e.checked = true;
		      })  
	         }
		}else if(len==2){
			if(str3){
		      $("."+str3).each(function(i,e){
				  $(e).parent('label').parent('dt').parent('dl').parent('div').parent('td').prev('th').find('label').find('input').each(function(ii,ee){
					  ee.checked = true;																					  
			      })		
			      e.checked = true;
		      })  
	        }
			if(str2){
		      $("."+str2).each(function(i,e){		
			      e.checked = true;
		      })  
	        }
	   }else if(len==3){
			if(str3){
		      $("."+str3).each(function(i,e){
				  $(e).parent('label').parent('dd').parent('dl').parent('div').parent('td').prev('th').find('label').find('input').each(function(ii,ee){
					  ee.checked = true;																					  
				  })
				  $(e).parent('label').parent('dd').prev('dt').find('label').find('input').each(function(iii,eee){
					  eee.checked = true;																					  
				  })
			      e.checked = true;
		      })  
	        }
	   }
	}else{
	   $("."+str3).each(function(i,e){
			  e.checked = false;
	   })  
	}
};

//提交按钮处理
function btn_is_disabled_obj(i,obj){
	if(i==true){
		$(obj).removeClass('disabled');
		$("input[type=text]").attr('disabled', false);
	}else{
		$(obj).addClass('disabled');
		$("input[type=text]").attr('disabled', 'disabled');
	}
}

//aajx提交，数据  地址 操作对象，是否选中  是否跳转  是否提示  是否返回提示
function ajaxpost(data,url,obj,isck,isback,istip,returnmsg){
	if(!isck){
	if(data['id'] ==false){
		window.top.msg(0,NO_CK_TIP);
		return false;
	}
	}
	btn_is_disabled_obj(false,obj);
	if(!istip){
		window.top.art.dialog({
    		content: PL_TIP,
    		ok: function () {
    			$.post(url,data,function(msg){
					if(msg.status){
				    	if(!isback)window.location.reload();//刷新
						if(!returnmsg) window.top.msg(msg.status,msg.message);
			    	}else{
						 window.top.msg(msg.status,msg.message);
					}
				    btn_is_disabled_obj(true,obj);			
				})
        		return true;
    		},
			title:TIP_INFO,
			lock:true,
			background:'#FFF',
			icon:'question',
			close:function(){
	     		btn_is_disabled_obj(true,obj);		
			},
    		cancelVal: CANCEL,
    		cancel: true //为true等价于function(){}
		});
  }else{
	 $.post(url,data,function(msg){
					//alert(msg)
					if(msg.status){
				    	if(!isback)window.location.reload();//刷新
						if(!returnmsg) window.top.msg(msg.status,msg.message);
			    	}else{
						 window.top.msg(msg.status,msg.message);
					}
				    btn_is_disabled_obj(true,obj);					
				})
        		return true;
	 }
}