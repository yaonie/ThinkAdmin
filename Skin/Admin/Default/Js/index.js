// JavaScript Document
$(function(){
     websize(); //窗口初始化
	
	 //用户滑过下拉
    $("#header_login_later").hover(function() {
        $(this).find('.fl').find('#J_head_user_menu').show();
    },
    function() {
        $(this).find('.fl').find('#J_head_user_menu').hide();
    });
	
	 //一级菜单输出
    var html = [];

    if (SUBMENU_CONFIG !== '') SUBMENU_CONFIG = jQuery.parseJSON(SUBMENU_CONFIG);
    $.each(SUBMENU_CONFIG,
    function(i, o) {
        html.push('<li style="color:' + o.menu_color + '; font-weight:' + o.menu_weight + ';" title="' + o.menu_name + '" data-id="' + o.menu_id + '" >' + o.menu_name + '</li>');
    });
    $('.menu_one').html(html.join(''));
    $('.menu_one').on('click', 'li',
    function() {
        $('.menu_one li').removeClass('on');
        $(this).addClass('on');
        var data_id = $(this).attr('data-id'),
        data_list = SUBMENU_CONFIG[data_id],
        html = [],
        child_html = [],
        child_index = 0,
        menutwo = $('.menutwo');
        if (menutwo.attr('data-id') == data_id) { //已经在当前了
            return false;
        } else {
            function show_left_menu(data) {
                for (var attr in data) {
                    if (data[attr] && typeof(data[attr]) === 'object') {
                        if (!data[attr].menu_url && attr === 'menu_items') {
                            //子菜单添加识别属性
                            $.each(data[attr],
                            function(i, o) {
                                child_index++;
                                o.isChild = true;
                                o.child_index = child_index;
                            });
                        }
                        show_left_menu(data[attr]); //继续执行循环(筛选子菜单)
                    } else {
                        if (attr === 'menu_name') {
                            data.menu_url = data.menu_url ? data.menu_url: '';

                            if (! (data['isChild'])) {
                                //一级菜单
                                var str = '<li><div class="ico leftmenu_title ';
                                //alert(data.menu_items)
                                if (data.menu_items.length !== 0) {
                                    str += 'find findin';
                                }
								
                                str += '" data-id="' + data.menu_id + '" style="color:' + data.menu_color + '; font-weight:' + data.menu_weight + ';" link="' + data.menu_url + '" >' + data.menu_name + '</div>'
								html.push(str);
                            } else {
								
                                //二级菜单
                                child_html.push('<li><div class="ico leftmenu_title" style="color:' + data.menu_color + '; font-weight:' + data.menu_weight + ';" link="' + data.menu_url + '" data-id="' + data.menu_id + '">' + data.menu_name + '</div></li>');
                                //二级菜单全部push完毕
                                if (data.child_index == child_index) {
                                    html.push('<ul class="menutree">' + child_html.join('') + '</ul>');
                                    child_html = [];
                                }
                            }
                        }

                    }
                }
            };
            show_left_menu(data_list['menu_items']);
            menutwo.html(html.join('')).attr('data-id', data_id);
            isscroll();
            left_click();
        }
    });
	$('.menu_one li:first').click(); //后台位在第一个导航
	$(window).resize(function() {
        websize();
    });
	
	//顶部点击一个tab页
    $('#B_history').on('click', 'li',
    function() {
        var data_id = $(this).data('id');
        $(this).addClass('current').siblings('li').removeClass('current');
        $('#iframe_' + data_id).show().siblings('iframe').hide(); //隐藏其它iframe
    });

    //下一个选项卡
    $('#J_next').click(function(e) {
        var ul = $('#B_history'),
        current = ul.find('.current'),
        li = current.next('li');
        showTab(li);
        return false;
    });

    //上一个选项卡
    $('#J_prev').click(function(e) {
        var ul = $('#B_history'),
        current = ul.find('.current'),
        li = current.prev('li');
        showTab(li);
        return false;
    });

    //顶部关闭一个tab页
    $('#B_history').on('click', 'a.del',
    function(e) {
        var li = $(this).parent().parent(),
        prev_li = li.prev('li'),
        data_id = li.attr('data-id');
        li.hide(60,
        function() {
            $(this).remove(); //移除选项卡
            $('#iframe_' + data_id).remove(); //移除iframe页面
            var current_li = $('#B_history li.current');
            //找到关闭后当前应该显示的选项卡
            current_li = current_li.length ? current_li: prev_li;
            current_li.addClass('current');
            cur_data_id = current_li.attr('data-id');
            $('#iframe_' + cur_data_id).show();
        });
    });
	
	/*
     * 搜索
     */
    $("#search_keyword").on('keypress',
    function() {
        if (event.keyCode == "13") {
            searchmenu();
        }
    });
	$("#searchmenu").on('click',
    function(e) {
        searchmenu();
    });
	
});

//窗口初始化
function websize() {
    var w = document.documentElement.clientWidth,
    h = document.documentElement.clientHeight;
    $("#footer_k,#left_k").height(h - $("#header").height());
	if($("#search_menu_k").length){
		search_menu_height = $("#search_menu_k").height();
	}else{
		search_menu_height = 0;  
	}
    $("#left_menu").height(h - $("#header").height() - search_menu_height - 40);
    $(".menutwo").animate({
        top: '0px'
    },
    "slow",
    function() {
        isscroll();
    });
    $("#right_k").width(w - 150);
    $(".main_k,.main").width(w - 150);
    $(".main_k,.main").height(h - $("#header").height() - 35);
    $("#navbarkt").width(w - 150 - 60);
    isscroll();
}


//判断显示滚动
function isscroll() {
    if ($("#left_menu").length) {
        var wai = $("#left_menu").height(),
        //外部区域高度
        nei = $(".menutwo").height(),
        //内部高度
        pianyi = wai,
        //每次允许移动的偏移量
        dangqianyd = $(".menutwo").position().top; // 当前已经移动了多少
        if (nei > wai) {
            if (nei - wai + dangqianyd <= 0) { //不能再下移动
                $("#roll_k li .nex_scoll").parent('li').addClass('nex_disabled');
            } else { //可以向下移动
                $("#roll_k li .nex_scoll").parent('li').removeClass('nex_disabled');
            }
            if (dangqianyd >= 0) {
                $("#roll_k li .pre_scoll").parent('li').addClass('pre_disabled');
            } else {
                $("#roll_k li .pre_scoll").parent('li').removeClass('pre_disabled');
            }
            $("#roll_k").show();
        } else {
            if (dangqianyd == 0) {
                $("#roll_k").hide();
            }
        }

    }

}

//向下滚动
function nex_scoll() {
    var wai = $("#left_menu").height(),
    //外部区域高度
    nei = $(".menutwo").height(),
    //内部高度
    pianyi = wai,
    //每次允许移动的偏移量
    dangqianyd = $(".menutwo").position().top; // 当前已经移动了多少
    if (nei > wai || dangqianyd > 0) {
        if (nei - wai + dangqianyd > 0) {
            if (nei - wai + dangqianyd > pianyi) {
                $(".menutwo").animate({
                    top: (dangqianyd - pianyi) + 'px'
                },
                "slow",
                function() {
                    isscroll();
                });
            } else {
                $(".menutwo").animate({
                    top: (dangqianyd - (nei - wai + dangqianyd)) + 'px'
                },
                "slow",
                function() {

                    isscroll();
                });
            }

        }
    }
}

//向下滚动
function pre_scoll() {
    var wai = $("#left_menu").height(),
    //外部区域高度
    nei = $(".menutwo").height(),
    //内部高度
    pianyi = wai,
    //每次允许移动的偏移量
    dangqianyd = $(".menutwo").position().top; // 当前已经移动了多少
    if (nei > wai || dangqianyd < 0) {
        if (dangqianyd < 0) {
            if ((0 - dangqianyd) > pianyi) {
                $(".menutwo").animate({
                    top: (dangqianyd + pianyi) + 'px'
                },
                "slow",
                function() {
                    isscroll();
                });
            } else {
                $(".menutwo").animate({
                    top: '0px'
                },
                "slow",
                function() {
                    isscroll();
                });
            }

        }
    }
}

function refreshs() {
    window.location.reload();
}; //刷新页面

//左边菜单点击
function left_click() {
    //左边菜单点击
    $('.menutwo li .leftmenu_title').click(function() {
        if ($(this).next('ul').length == 0) {
            $('.menutwo li').removeClass('on');
            $(this).parent().addClass('on');
            var links = $(this).attr('link'); //获取链接
            $('#B_history li').removeClass('current');
            var data_id = $(this).attr('data-id'),
            $this = $(this),
            li = $('#B_history li[data-id=' + data_id + ']');
            iframeJudge({
                elem: $this,
                href: links,
                id: data_id
            });
        } else { //存在子集
            var obj = $(this).next('ul');
            obj.animate({
                height: 'toggle',
                opacity: 'toggle'
            },
            function() {
                if (obj.is(":hidden")) {
                    obj.prev().removeClass("findin");
                } else {
                    obj.prev().addClass("findin");
                }
                isscroll();
            });
        }
    })
}


//判断显示或创建iframe
function iframeJudge(options) {
    var elem = options.elem,
    href = options.href,
    id = options.id,
    li = $('#B_history li[data-id=' + id + ']');
    if (id == 'search' && li.length > 0) { //如果是搜索
        //如果是已经存在的iframe，则显示并让选项卡高亮,并不显示loading
        li.addClass('current');
        $('#B_frame iframe').hide();
        $('#iframe_' + id).show();
        $('#iframe_' + id).attr('src', href);
        showTab(li); //计算此tab的位置，如果不在屏幕内，则移动导航位置
    }
    if (li.length > 0) {
        //如果是已经存在的iframe，则显示并让选项卡高亮,并不显示loading
        li.addClass('current');
        $('#B_frame iframe').hide();
        $('#iframe_' + id).show();
        showTab(li); //计算此tab的位置，如果不在屏幕内，则移动导航位置
    } else {
        //创建一个并加以标识
        var iframeAttr = {
            src: href,
            id: 'iframe_' + id,
            name: 'iframe_' + id,
            frameborder: '0',
            height: '100%',
            scrolling: 'auto',
            width: '100%'
        };
        $('#B_frame iframe').hide();
        var iframe = $("<iframe/>").prop(iframeAttr).appendTo("#B_frame");
        $(iframe[0].contentWindow.document).ready(function() {
            var li = $('<li tabindex="0"><span><a>' + elem.html() + '</a><a class="ico del" title="关闭此页">关闭</a></span></li>').attr('data-id', id).addClass('current');
            li.siblings().removeClass('current');
            li.appendTo('#B_history');
            showTab(li); //计算此tab的位置，如果不在屏幕内，则移动导航位置
        });

    }
}

//显示顶部导航时作位置判断，点击左边菜单、上一tab、下一tab时公用
function showTab(li) {
    if (li.length) {
        var ul = $('#B_history'),
        li_offset = li.offset(),
        li_width = li.outerWidth(true),
        next_left = $('#J_next').offset().left - 9,
        //右边按钮的界限位置
        prev_right = $('#J_prev').offset().left + $('#J_prev').outerWidth(true); //左边按钮的界限位置
        if (li_offset.left + li_width > next_left) { //如果将要移动的元素在不可见的右边，则需要移动
            var distance = li_offset.left + li_width - next_left; //计算当前父元素的右边距离，算出右移多少像素
            ul.animate({
                left: '-=' + distance
            },
            200, 'swing');
        } else if (li_offset.left < prev_right) { //如果将要移动的元素在不可见的左边，则需要移动
            var distance = prev_right - li_offset.left; //计算当前父元素的左边距离，算出左移多少像素
            ul.animate({
                left: '+=' + distance
            },
            200, 'swing');
        }
        li.trigger('click');
    }
}

//刷新内框架函数
function refreshfind(url) {
    var id = $('#B_history .current').attr('data-id'),
    iframe = $('#iframe_' + id);
    if (!url) {
        url = iframe[0].contentWindow.window.location.href
    }
    $(iframe[0]).attr('src', url);
}

//搜索
function searchmenu() {
    var $this = $("#searchmenu"),
    search_val = $.trim($("#search_keyword").val());
    if (search_val) {
        iframeJudge({
            elem: $this,
            href: $this.data('url') + '&keyword=' + search_val,
            id: 'search'
        });
    }
}

//获取内框架win
function findiframe(obj) {
    var id = $('#B_history .current').attr('data-id'),
    iframe = $('#iframe_' + id);
    return iframe[0].contentWindow;
}

//清空缓存
function deletecache(url) {
    $.post(url,{posttype:'delete_cacheall'},
    function(data) {
        msg(data.status, data.message)
    })
}

//弹框组件
function artiframe(url, title, width, height, lock, resize, background, button, id, fixeds, closefun, left, top, padding) {
    if (!width) width = 'auto';
    if (!height) height = 'auto';
    if (!lock) lock = false;
    if (!resize) resize = false;
    if (!background) background = '#FFF';
    if (!closefun) closefun = null;
    if (!button) button = [{
        name: CLOSE
    }];
    if (!left) left = '50%';
    if (!top) top = '38.2%';
    if (!id) id = null;
    if (!fixeds) fixeds = false;
    if (!padding) padding = 0;
    art.dialog.open(url, {
        init: function() {
            var iframe = this.iframe.contentWindow;
            window.top.art.dialog.data('iframe' + id, iframe);
        },
        id: id,
        title: title,
        padding: padding,
        width: width,
        height: height,
        lock: lock,
        resize: resize,
        background: background,
        button: button,
        fixed: fixeds,
        close: closefun,
        left: left,
        top: top

    });
}

//子框架打开
function findopen(url, obj, type) {
    var $this = $(obj);
    iframeJudge({
        elem: $this,
        href: url,
        id: type
    });
}